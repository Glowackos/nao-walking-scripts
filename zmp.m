function [x_zmp, y_zmp, KE] = ZMP( ...
    a_r_r, a_p_r, k_p_r, h_r_r, h_p_r, ...
    da_r_r, da_p_r, dk_p_r, dh_r_r, dh_p_r, ...
    dda_r_r, dda_p_r, ddk_p_r, ddh_r_r, ddh_p_r, ...
    a_r_l, a_p_l, k_p_l, h_r_l, h_p_l, ...
    da_r_l, da_p_l, dk_p_l, dh_r_l, dh_p_l, ...
    dda_r_l, dda_p_l, ddk_p_l, ddh_r_l, ddh_p_l)
%ZMP Returns zmp trajectory for half-step, assuming right foot is supporting.
% The entire upper body, down to and including the hip joints, is treated as a
% single rigid mass.

% NOTE: Matrix multiplication is intentionally avoided. If we were to use
% it, we wouldn't be able to pass in time vectors as the arguments.

g = 9.80665; % m/s^2

NeckOffsetZ = 0.1265;
UpperArmLength = 0.105;
LowerArmLength = 0.05595;
HandOffsetX = 0.05775;
HipOffsetZ = 0.085;
HipOffsetY = 0.05;
ThighLength = 0.1;
TibiaLength = 0.1029;
FootHeight = 0.04519;

sarr = @(t) sin(a_r_r(t));
dsarr = @(t) cos(a_r_r(t)).*da_r_r(t);
ddsarr = @(t) cos(a_r_r(t)).*dda_r_r(t) - sin(a_r_r(t)).*da_r_r(t).^2;
sapr = @(t) sin(a_p_r(t));
dsapr = @(t) cos(a_p_r(t)).*da_p_r(t);
ddsapr = @(t) cos(a_p_r(t)).*dda_p_r(t) - sin(a_p_r(t)).*da_p_r(t).^2;
skpr = @(t) sin(k_p_r(t));
dskpr = @(t) cos(k_p_r(t)).*dk_p_r(t);
ddskpr = @(t) cos(k_p_r(t)).*ddk_p_r(t) - sin(k_p_r(t)).*dk_p_r(t).^2;
shrr = @(t) sin(h_r_r(t));
dshrr = @(t) cos(h_r_r(t)).*dh_r_r(t);
ddshrr = @(t) cos(h_r_r(t)).*ddh_r_r(t) - sin(h_r_r(t)).*dh_r_r(t).^2;
shpr = @(t) sin(h_p_r(t));
dshpr = @(t) cos(h_p_r(t)).*dh_p_r(t);
ddshpr = @(t) cos(h_p_r(t)).*ddh_p_r(t) - sin(h_p_r(t)).*dh_p_r(t).^2;

carr = @(t) cos(a_r_r(t));
dcarr = @(t) -sin(a_r_r(t)).*da_r_r(t);
ddcarr = @(t) -sin(a_r_r(t)).*dda_r_r(t) - cos(a_r_r(t)).*da_r_r(t).^2;
capr = @(t) cos(a_p_r(t));
dcapr = @(t) -sin(a_p_r(t)).*da_p_r(t);
ddcapr = @(t) -sin(a_p_r(t)).*dda_p_r(t) - cos(a_p_r(t)).*da_p_r(t).^2;
ckpr = @(t) cos(k_p_r(t));
dckpr = @(t) -sin(k_p_r(t)).*dk_p_r(t);
ddckpr = @(t) -sin(k_p_r(t)).*ddk_p_r(t) - cos(k_p_r(t)).*dk_p_r(t).^2;
chrr = @(t) cos(h_r_r(t));
dchrr = @(t) -sin(h_r_r(t)).*dh_r_r(t);
ddchrr = @(t) -sin(h_r_r(t)).*ddh_r_r(t) - cos(h_r_r(t)).*dh_r_r(t).^2;
chpr = @(t) cos(h_p_r(t));
dchpr = @(t) -sin(h_p_r(t)).*dh_p_r(t);
ddchpr = @(t) -sin(h_p_r(t)).*ddh_p_r(t) - cos(h_p_r(t)).*dh_p_r(t).^2;

sarl = @(t) sin(a_r_l(t));
dsarl = @(t) cos(a_r_l(t)).*da_r_l(t);
ddsarl = @(t) cos(a_r_l(t)).*dda_r_l(t) - sin(a_r_l(t)).*da_r_l(t).^2;
sapl = @(t) sin(a_p_l(t));
dsapl = @(t) cos(a_p_l(t)).*da_p_l(t);
ddsapl = @(t) cos(a_p_l(t)).*dda_p_l(t) - sin(a_p_l(t)).*da_p_l(t).^2;
skpl = @(t) sin(k_p_l(t));
dskpl = @(t) cos(k_p_l(t)).*dk_p_l(t);
ddskpl = @(t) cos(k_p_l(t)).*ddk_p_l(t) - sin(k_p_l(t)).*dk_p_l(t).^2;
shrl = @(t) sin(h_r_l(t));
dshrl = @(t) cos(h_r_l(t)).*dh_r_l(t);
ddshrl = @(t) cos(h_r_l(t)).*ddh_r_l(t) - sin(h_r_l(t)).*dh_r_l(t).^2;
shpl = @(t) sin(h_p_l(t));
dshpl = @(t) cos(h_p_l(t)).*dh_p_l(t);
ddshpl = @(t) cos(h_p_l(t)).*ddh_p_l(t) - sin(h_p_l(t)).*dh_p_l(t).^2;

carl = @(t) cos(a_r_l(t));
dcarl = @(t) -sin(a_r_l(t)).*da_r_l(t);
ddcarl = @(t) -sin(a_r_l(t)).*dda_r_l(t) - cos(a_r_l(t)).*da_r_l(t).^2;
capl = @(t) cos(a_p_l(t));
dcapl = @(t) -sin(a_p_l(t)).*da_p_l(t);
ddcapl = @(t) -sin(a_p_l(t)).*dda_p_l(t) - cos(a_p_l(t)).*da_p_l(t).^2;
ckpl = @(t) cos(k_p_l(t));
dckpl = @(t) -sin(k_p_l(t)).*dk_p_l(t);
ddckpl = @(t) -sin(k_p_l(t)).*ddk_p_l(t) - cos(k_p_l(t)).*dk_p_l(t).^2;
chrl = @(t) cos(h_r_l(t));
dchrl = @(t) -sin(h_r_l(t)).*dh_r_l(t);
ddchrl = @(t) -sin(h_r_l(t)).*ddh_r_l(t) - cos(h_r_l(t)).*dh_r_l(t).^2;
chpl = @(t) cos(h_p_l(t));
dchpl = @(t) -sin(h_p_l(t)).*dh_p_l(t);
ddchpl = @(t) -sin(h_p_l(t)).*ddh_p_l(t) - cos(h_p_l(t)).*dh_p_l(t).^2;


%%%%%%%%%
% Torso %
%%%%%%%%%
% Since we have symmetry in the Y axis, all com entries can have y=0.
m_Torso_ = 1.03948;
com_Torso_ = [-0.00415 0 0.04258]';
m_Neck = 0.05930;
com_Neck = [0 0 NeckOffsetZ]' + [-0.00002 0 -0.02556]';
m_Head = 0.52065;
com_Head = [0 0 NeckOffsetZ]' + [0.0012 0 0.05353]';
m_Shoulder = 0.06996;
com_Shoulder = [0 0 0.1]' + [0.00018 0 0.00178]';
m_Bicep = 0.12309;
com_Bicep = [0 0 0.1]' + [0.00065 0 -0.01885]';
m_Elbow = 0.05971;
com_Elbow = [0 0 0.1-UpperArmLength]' + [-0.00019 0 0.0256]';
m_ForeArm = 0.07724;
com_ForeArm = [0 0 0.1-UpperArmLength-LowerArmLength]' + [0.00096 0 -0.02556]';
m_Hands = 0.16653;
com_Hands = [0 0 0.1-UpperArmLength-LowerArmLength-HandOffsetX]' + [-0.00083 0 -0.0318]';
m_Pelvis = 0.07117;
com_Pelvis = [0 0 -HipOffsetZ]' + [-0.00766 0 0.02717]';

m_Torso = m_Torso_ + m_Neck + m_Head + ...
    2*(m_Shoulder + m_Bicep + m_Elbow + m_ForeArm + m_Hands + m_Pelvis);
com_Torso = (m_Torso_*com_Torso_ + m_Neck*com_Neck + m_Head*com_Head + ...
    2*(m_Shoulder*com_Shoulder + ...
    m_Bicep*com_Bicep + ...
    m_Elbow*com_Elbow + ...
    m_ForeArm*com_ForeArm + ...
    m_Hands*com_Hands + ...
    m_Pelvis*com_Pelvis)) / m_Torso;
% Get cooordinates and their derivatives with reference to the support foot.
% >> FK_sym; simplify((T_Torso_RL0*T_L01*T_L12*T_L23*T_L34*T_L45*T_L5E)^-1)
% [ 1, 0, 0,                                                                                         TibiaLength*sin(h_p + k_p) + ThighLength*sin(h_p)]
% [ 0, 1, 0,              HipOffsetY - ThighLength*cos(h_p)*sin(h_r) - TibiaLength*cos(h_p)*cos(k_p)*sin(h_r) + TibiaLength*sin(h_p)*sin(h_r)*sin(k_p)]
% [ 0, 0, 1, FootHeight + HipOffsetZ + ThighLength*cos(h_p)*cos(h_r) + TibiaLength*cos(h_p)*cos(h_r)*cos(k_p) - TibiaLength*cos(h_r)*sin(h_p)*sin(k_p)]
% [ 0, 0, 0,                                                                                                                                         1]
x_Torso = @(t) com_Torso(1) - TibiaLength*sapr(t) + ThighLength*shpr(t);
dx_Torso = @(t) -TibiaLength*dsapr(t) + ThighLength*dshpr(t);
ddx_Torso = @(t) -TibiaLength*ddsapr(t) + ThighLength*ddshpr(t);
y_Torso = @(t) com_Torso(2) + HipOffsetY - TibiaLength*shrr(t).*capr(t) - ThighLength*shrr(t).*chpr(t);
dy_Torso = @(t) -TibiaLength*(dshrr(t).*capr(t) + shrr(t).*dcapr(t)) - ThighLength*(dshrr(t).*chpr(t) + shrr(t).*dchpr(t));
ddy_Torso = @(t) -TibiaLength*(ddshrr(t).*capr(t) + 2*dshrr(t).*dcapr(t) + shrr(t).*ddcapr(t)) - ThighLength*(ddshrr(t).*chpr(t) + 2*dshrr(t).*dchpr(t) + shrr(t).*ddchpr(t));
z_Torso = @(t) com_Torso(3) + FootHeight + HipOffsetZ + TibiaLength*chrr(t).*capr(t) + ThighLength*chrr(t).*chpr(t);
dz_Torso = @(t) TibiaLength*(dchrr(t).*capr(t) + chrr(t).*dcapr(t)) + ThighLength*(dchrr(t).*chpr(t) + chrr(t).*dchpr(t));
ddz_Torso = @(t) TibiaLength*(ddchrr(t).*capr(t) + 2*dchrr(t).*dcapr(t) + chrr(t).*ddcapr(t)) + ThighLength*(ddchrr(t).*chpr(t) + 2*dchrr(t).*dchpr(t) + chrr(t).*ddchpr(t));

x1 = @(t) m_Torso * (ddz_Torso(t) + g) .* x_Torso(t);
x2 = @(t) m_Torso * ddx_Torso(t) .* z_Torso(t);
x3 = @(t) m_Torso * (ddz_Torso(t) + g);
y1 = @(t) m_Torso * (ddy_Torso(t) + g) .* y_Torso(t);
y2 = @(t) m_Torso * ddy_Torso(t) .* z_Torso(t);
y3 = @(t) m_Torso * (ddz_Torso(t) + g);
KE = @(t) m_Torso * (dx_Torso(t).^2 + dy_Torso(t).^2 + dz_Torso(t).^2) / 2;


%%%%%%%%%%%%%%%%%%%%%%%%%%
% Left Thigh (Frame LL2) %
%%%%%%%%%%%%%%%%%%%%%%%%%%
m_LL2 = 0.39421;
com_LL2 = [0.05352 -0.00132 0.00235]';
% Get cooordinates and their derivatives with reference to the support foot.
% >> FK_sym; simplify((T_Torso_RL0*T_L01*T_L12*T_L23*T_L34*T_L45*T_L5E)^-1) % using right leg angles.
% [ 1, 0, 0,                                                                                         TibiaLength*sin(h_p + k_p) + ThighLength*sin(h_p)]
% [ 0, 1, 0,              HipOffsetY - ThighLength*cos(h_p)*sin(h_r) - TibiaLength*cos(h_p)*cos(k_p)*sin(h_r) + TibiaLength*sin(h_p)*sin(h_r)*sin(k_p)]
% [ 0, 0, 1, FootHeight + HipOffsetZ + ThighLength*cos(h_p)*cos(h_r) + TibiaLength*cos(h_p)*cos(h_r)*cos(k_p) - TibiaLength*cos(h_r)*sin(h_p)*sin(k_p)]
% [ 0, 0, 0,                                                                                                                                         1]
% >> simplify(T_Torso_LL0*T_L01*T_L12) % using left leg angles.
% [          -sin(h_p),          -cos(h_p),        0,           0]
% [  cos(h_p)*sin(h_r), -sin(h_p)*sin(h_r), cos(h_r),  HipOffsetY]
% [ -cos(h_p)*cos(h_r),  cos(h_r)*sin(h_p), sin(h_r), -HipOffsetZ]
% [                  0,                  0,        0,           1]
x_LL2 = @(t) -shpl(t)*com_LL2(1) - chpl(t)*com_LL2(2) - TibiaLength*sapr(t) + ThighLength*shpr(t);
dx_LL2 = @(t) -dshpl(t)*com_LL2(1) - dchpl(t)*com_LL2(2) + dx_Torso(t);
ddx_LL2 = @(t) -ddshpl(t)*com_LL2(1) - ddchpl(t)*com_LL2(2) + ddx_Torso(t);
y_LL2 = @(t) chpl(t).*shrl(t)*com_LL2(1) - shpl(t).*shrl(t)*com_LL2(2) + chrl(t)*com_LL2(3) + 2*HipOffsetY - TibiaLength*shrr(t).*capr(t) - ThighLength*shrr(t).*chpr(t);
dy_LL2 = @(t) (dchpl(t).*shrl(t) + chpl(t).*dshrl(t))*com_LL2(1) - (dshpl(t).*shrl(t) + shpl(t).*dshrl(t))*com_LL2(2) + dchrl(t)*com_LL2(3) + dy_Torso(t);
ddy_LL2 = @(t) (ddchpl(t).*shrl(t) + 2*dchpl(t).*dshrl(t) + chpl(t).*ddshrl(t))*com_LL2(1) - (ddshpl(t).*shrl(t) + 2*dshpl(t).*dshrl(t) + shpl(t).*ddshrl(t))*com_LL2(2) + ddchrl(t)*com_LL2(3) + ddy_Torso(t);
z_LL2 = @(t) -chpl(t).*chrl(t)*com_LL2(1) + shpl(t).*chrl(t)*com_LL2(2) + shrl(t)*com_LL2(3) + FootHeight + TibiaLength*chrr(t).*capr(t) + ThighLength*chrr(t).*chpr(t);
dz_LL2 = @(t) -(dchpl(t).*chrl(t) + chpl(t).*dchrl(t))*com_LL2(1) + (dshpl(t).*chrl(t) + shpl(t).*dchrl(t))*com_LL2(2) + dshrl(t)*com_LL2(3) + dz_Torso(t);
ddz_LL2 = @(t) -(ddchpl(t).*chrl(t) + 2*dchpl(t).*dchrl(t) + chpl(t).*ddchrl(t))*com_LL2(1) + (ddshpl(t).*chrl(t) + 2*dshpl(t).*dchrl(t) + shpl(t).*ddchrl(t))*com_LL2(2) + ddshrl(t)*com_LL2(3) + ddz_Torso(t);

x1 = @(t) x1(t) + m_LL2 * (ddz_LL2(t) + g) .* x_LL2(t);
x2 = @(t) x2(t) + m_LL2 * ddx_LL2(t) .* z_LL2(t);
x3 = @(t) x3(t) + m_LL2 * (ddz_LL2(t) + g);
y1 = @(t) y1(t) + m_LL2 * (ddy_LL2(t) + g) .* y_LL2(t);
y2 = @(t) y2(t) + m_LL2 * ddy_LL2(t) .* z_LL2(t);
y3 = @(t) y3(t) + m_LL2 * (ddz_LL2(t) + g);
KE = @(t) KE(t) + m_LL2 * (dx_LL2(t).^2 + dy_LL2(t).^2 + dz_LL2(t).^2) / 2;


%%%%%%%%%%%%%%%%%%%%%%%%%%
% Left Tibia (Frame LL3) %
%%%%%%%%%%%%%%%%%%%%%%%%%%
m_LL3 = 0.29159;
com_LL3 = [0.04868 -0.00252 0.00252]';
% Get cooordinates and their derivatives with reference to the support foot.
% >> FK_sym; simplify((T_Torso_RL0*T_L01*T_L12*T_L23*T_L34*T_L45*T_L5E)^-1) % using right leg angles.
% [ 1, 0, 0,                                                                                         TibiaLength*sin(h_p + k_p) + ThighLength*sin(h_p)]
% [ 0, 1, 0,              HipOffsetY - ThighLength*cos(h_p)*sin(h_r) - TibiaLength*cos(h_p)*cos(k_p)*sin(h_r) + TibiaLength*sin(h_p)*sin(h_r)*sin(k_p)]
% [ 0, 0, 1, FootHeight + HipOffsetZ + ThighLength*cos(h_p)*cos(h_r) + TibiaLength*cos(h_p)*cos(h_r)*cos(k_p) - TibiaLength*cos(h_r)*sin(h_p)*sin(k_p)]
% [ 0, 0, 0,                                                                                                                                         1]
% >> simplify(T_Torso_LL0*T_L01*T_L12*T_L23) % using left leg angles.
% [          -sin(h_p + k_p),          -cos(h_p + k_p),        0,                        -ThighLength*sin(h_p)]
% [  cos(h_p + k_p)*sin(h_r), -sin(h_p + k_p)*sin(h_r), cos(h_r),   HipOffsetY + ThighLength*cos(h_p)*sin(h_r)]
% [ -cos(h_p + k_p)*cos(h_r),  sin(h_p + k_p)*cos(h_r), sin(h_r), - HipOffsetZ - ThighLength*cos(h_p)*cos(h_r)]
% [                        0,                        0,        0,                                            1]
x_LL3 = @(t) sapl(t)*com_LL3(1) - capl(t)*com_LL3(2) - ThighLength*shpl(t) - TibiaLength*sapr(t) + ThighLength*shpr(t);
dx_LL3 = @(t) dsapl(t)*com_LL3(1) - dcapl(t)*com_LL3(2) - ThighLength*dshpl(t) + dx_Torso(t);
ddx_LL3 = @(t) ddsapl(t)*com_LL3(1) - ddcapl(t)*com_LL3(2) - ThighLength*ddshpl(t) + ddx_Torso(t);
y_LL3 = @(t) capl(t).*shrl(t)*com_LL3(1) + sapl(t).*shrl(t)*com_LL3(2) + chrl(t)*com_LL3(3) + ThighLength*chpl(t).*shrl(t) + 2*HipOffsetY - TibiaLength*shrr(t).*capr(t) - ThighLength*shrr(t).*chpr(t);
dy_LL3 = @(t) (dcapl(t).*shrl(t) + capl(t).*dshrl(t))*com_LL3(1) + (dsapl(t).*shrl(t) + sapl(t).*dshrl(t))*com_LL3(2) + dchrl(t)*com_LL3(3) + ThighLength*(dchpl(t).*shrl(t) + chpl(t).*dshrl(t)) + dy_Torso(t);
ddy_LL3 = @(t) (ddcapl(t).*shrl(t) + 2*dcapl(t).*dshrl(t) + capl(t).*ddshrl(t))*com_LL3(1) + (ddsapl(t).*shrl(t) + 2*dsapl(t).*dshrl(t) + sapl(t).*ddshrl(t))*com_LL3(2) + ddchrl(t)*com_LL3(3) + ThighLength*(ddchpl(t).*shrl(t) + 2*dchpl(t).*dshrl(t) + chpl(t).*ddshrl(t)) + ddy_Torso(t);
z_LL3 = @(t) -capl(t).*chrl(t)*com_LL3(1) - sapl(t).*chrl(t)*com_LL3(2) + shrl(t)*com_LL3(3) - ThighLength*chpl(t).*chrl(t) + FootHeight + TibiaLength*chrr(t).*capr(t) + ThighLength*chrr(t).*chpr(t);
dz_LL3 = @(t) -(dcapl(t).*chrl(t) + capl(t).*dchrl(t))*com_LL3(1) - (dsapl(t).*chrl(t) + sapl(t).*dchrl(t))*com_LL3(2) + dshrl(t)*com_LL3(3) - ThighLength*(dchpl(t).*chrl(t) + chpl(t).*dchrl(t)) + dz_Torso(t);
ddz_LL3 = @(t) -(ddcapl(t).*chrl(t) + 2*dcapl(t).*dchrl(t) + capl(t).*ddchrl(t))*com_LL3(1) - (ddsapl(t).*chrl(t) + 2*dsapl(t).*dchrl(t) + sapl(t).*ddchrl(t))*com_LL3(2) + ddshrl(t)*com_LL3(3) - ThighLength*(ddchpl(t).*chrl(t) + 2*dchpl(t).*dchrl(t) + chpl(t).*ddchrl(t)) + ddz_Torso(t);

x1 = @(t) x1(t) + m_LL3 * (ddz_LL3(t) + g) .* x_LL3(t);
x2 = @(t) x2(t) + m_LL3 * ddx_LL3(t) .* z_LL3(t);
x3 = @(t) x3(t) + m_LL3 * (ddz_LL3(t) + g);
y1 = @(t) y1(t) + m_LL3 * (ddy_LL3(t) + g) .* y_LL3(t);
y2 = @(t) y2(t) + m_LL3 * ddy_LL3(t) .* z_LL3(t);
y3 = @(t) y3(t) + m_LL3 * (ddz_LL3(t) + g);
KE = @(t) KE(t) + m_LL3 * (dx_LL3(t).^2 + dy_LL3(t).^2 + dz_LL3(t).^2) / 2;


%%%%%%%%%%%%%%%%%%%%%%%%%%
% Left Foot (Frame LLE) %
%%%%%%%%%%%%%%%%%%%%%%%%%%
m_ankle = 0.13892;
com_ankle = [0 0 FootHeight]' + [0.00142 0.00028 0.00638]';
m_foot = 0.16175;
com_foot = [0 0 FootHeight]' + [0.02540 0.00332 -0.03241]';
m_LLE = m_ankle + m_foot;
com_LLE = (m_ankle*com_ankle + m_foot*com_foot) / m_LLE;
% Get cooordinates and their derivatives with reference to the support foot.
% >> FK_sym; simplify((T_Torso_RL0*T_L01*T_L12*T_L23*T_L34*T_L45*T_L5E)^-1) % using right leg angles.
% [ 1, 0, 0,                                                                                         TibiaLength*sin(h_p + k_p) + ThighLength*sin(h_p)]
% [ 0, 1, 0,              HipOffsetY - ThighLength*cos(h_p)*sin(h_r) - TibiaLength*cos(h_p)*cos(k_p)*sin(h_r) + TibiaLength*sin(h_p)*sin(h_r)*sin(k_p)]
% [ 0, 0, 1, FootHeight + HipOffsetZ + ThighLength*cos(h_p)*cos(h_r) + TibiaLength*cos(h_p)*cos(h_r)*cos(k_p) - TibiaLength*cos(h_r)*sin(h_p)*sin(k_p)]
% [ 0, 0, 0,                                                                                                                                         1]
% >> simplify(T_Torso_LL0*T_L01*T_L12*T_L23*T_L34*T_L45*T_L5E) % using left leg angles.
% [ 1, 0, 0,                                                                                         - TibiaLength*sin(h_p + k_p) - ThighLength*sin(h_p)]
% [ 0, 1, 0,                HipOffsetY + ThighLength*cos(h_p)*sin(h_r) + TibiaLength*cos(h_p)*cos(k_p)*sin(h_r) - TibiaLength*sin(h_p)*sin(h_r)*sin(k_p)]
% [ 0, 0, 1,  -FootHeight - HipOffsetZ - ThighLength*cos(h_p)*cos(h_r) - TibiaLength*cos(h_p)*cos(h_r)*cos(k_p) + TibiaLength*cos(h_r)*sin(h_p)*sin(k_p)]
% [ 0, 0, 0,                                                                                                                                           1]
x_LLE = @(t) com_LLE(1) + TibiaLength*sapl(t) - ThighLength*shpl(t) - TibiaLength*sapr(t) + ThighLength*shpr(t);
dx_LLE = @(t) TibiaLength*dsapl(t) - ThighLength*dshpl(t) + dx_Torso(t);
ddx_LLE = @(t) TibiaLength*ddsapl(t) - ThighLength*ddshpl(t) + ddx_Torso(t);
y_LLE = @(t) com_LLE(2) + 2*HipOffsetY + TibiaLength*shrl(t).*capl(t) + ThighLength*shrl(t).*chpl(t) - TibiaLength*shrr(t).*capr(t) - ThighLength*shrr(t).*chpr(t);
dy_LLE = @(t) TibiaLength*(dshrl(t).*capl(t) + shrl(t).*dcapl(t)) + ThighLength*(dshrl(t).*chpl(t) + shrl(t).*dchpl(t)) + dy_Torso(t);
ddy_LLE = @(t) TibiaLength*(ddshrl(t).*capl(t) + 2*dshrl(t).*dcapl(t) + shrl(t).*ddcapl(t)) + ThighLength*(ddshrl(t).*chpl(t) + 2*dshrl(t).*dchpl(t) + shrl(t).*ddchpl(t)) + ddy_Torso(t);
z_LLE = @(t) com_LLE(3) - TibiaLength*chrl(t).*capl(t) - ThighLength*chrl(t).*chpl(t) + TibiaLength*chrr(t).*capr(t) + ThighLength*chrr(t).*chpr(t);
dz_LLE = @(t) -TibiaLength*(dchrl(t).*capl(t) + chrl(t).*dcapl(t)) - ThighLength*(dchrl(t).*chpl(t) + chrl(t).*dchpl(t)) + dz_Torso(t);
ddz_LLE = @(t) -TibiaLength*(ddchrl(t).*capl(t) + 2*dchrl(t).*dcapl(t) + chrl(t).*ddcapl(t)) - ThighLength*(ddchrl(t).*chpl(t) + 2*dchrl(t).*dchpl(t) + chrl(t).*ddchpl(t)) + ddz_Torso(t);

x1 = @(t) x1(t) + m_LLE * (ddz_LLE(t) + g) .* x_LLE(t);
x2 = @(t) x2(t) + m_LLE * ddx_LLE(t) .* z_LLE(t);
x3 = @(t) x3(t) + m_LLE * (ddz_LLE(t) + g);
y1 = @(t) y1(t) + m_LLE * (ddy_LLE(t) + g) .* y_LLE(t);
y2 = @(t) y2(t) + m_LLE * ddy_LLE(t) .* z_LLE(t);
y3 = @(t) y3(t) + m_LLE * (ddz_LLE(t) + g);
KE = @(t) KE(t) + m_LLE * (dx_LLE(t).^2 + dy_LLE(t).^2 + dz_LLE(t).^2) / 2;


%%%%%%%%%%%%%%%%%%%%%%%%%%
% Right Foot (Frame RLE) %
%%%%%%%%%%%%%%%%%%%%%%%%%%
m_ankle = 0.13892;
com_ankle = [0 0 FootHeight]' + [0.00142 -0.00028 0.00638]';
m_foot = 0.16175;
com_foot = [0 0 FootHeight]' + [0.02540 -0.00332 -0.03241]';
m_RLE = m_ankle + m_foot;
com_RLE = (m_ankle*com_ankle + m_foot*com_foot) / m_RLE;
% Get cooordinates and their derivatives with reference to the support foot.
% This one is easy as the frame doesn't move.
x_RLE = @(t) com_RLE(1);
dx_RLE = @(t) 0;
ddx_RLE = @(t) 0;
y_RLE = @(t) com_RLE(2);
dy_RLE = @(t) 0;
ddy_RLE = @(t) 0;
z_RLE = @(t) com_RLE(3);
dz_RLE = @(t) 0;
ddz_RLE = @(t) 0;

x1 = @(t) x1(t) + m_RLE * (ddz_RLE(t) + g) .* x_RLE(t);
x2 = @(t) x2(t) + m_RLE * ddx_RLE(t) .* z_RLE(t);
x3 = @(t) x3(t) + m_RLE * (ddz_RLE(t) + g);
y1 = @(t) y1(t) + m_RLE * (ddy_RLE(t) + g) .* y_RLE(t);
y2 = @(t) y2(t) + m_RLE * ddy_RLE(t) .* z_RLE(t);
y3 = @(t) y3(t) + m_RLE * (ddz_RLE(t) + g);
KE = @(t) KE(t) + m_RLE * (dx_RLE(t).^2 + dy_RLE(t).^2 + dz_RLE(t).^2) / 2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Right Tibia (Frame RL3) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%
m_RL3 = 0.29159;
com_RL3 = [0.04868 0.00252 0.00252]';
% Get cooordinates and their derivatives with reference to the support foot.
% >> FK_sym; simplify((T_Torso_RL0*T_L01*T_L12*T_L23*T_L34*T_L45*T_L5E)^-1) % using right leg angles.
% [           sin(a_p),          -cos(a_p),        0,                      -TibiaLength*sin(a_p)]
% [ -cos(a_p)*sin(a_r), -sin(a_p)*sin(a_r), cos(a_r),              TibiaLength*cos(a_p)*sin(a_r)]
% [ -cos(a_p)*cos(a_r), -sin(a_p)*cos(a_r), sin(h_r), FootHeight + TibiaLength*cos(a_p)*cos(a_r)]
% [                  0,                  0,        0,                                          1]
x_RL3 = @(t) sapr(t)*com_RL3(1) - capr(t)*com_RL3(2) - TibiaLength*sapr(t);
dx_RL3 = @(t) dsapr(t)*com_RL3(1) - dcapr(t)*com_RL3(2) - TibiaLength*dsapr(t);
ddx_RL3 = @(t) ddsapr(t)*com_RL3(1) - ddcapr(t)*com_RL3(2) - TibiaLength*ddsapr(t);
y_RL3 = @(t) -capr(t).*sarr(t)*com_RL3(1) - sapr(t).*sarr(t)*com_RL3(2) + carr(t)*com_RL3(3) + TibiaLength*capr(t).*sarr(t);
dy_RL3 = @(t) -(dcapr(t).*sarr(t) + capr(t).*dsarr(t))*com_RL3(1) - (dsapr(t).*sarr(t) + sapr(t).*dsarr(t))*com_RL3(2) + dcarr(t)*com_RL3(3) + TibiaLength*(dcapr(t).*sarr(t) + capr(t).*dsarr(t));
ddy_RL3 = @(t) -(ddcapr(t).*sarr(t) + 2*dcapr(t).*dsarr(t) + capr(t).*ddsarr(t))*com_RL3(1) - (ddsapr(t).*sarr(t) + 2*dsapr(t).*dsarr(t) + sapr(t).*ddsarr(t))*com_RL3(2) + ddcarr(t)*com_RL3(3) + TibiaLength*(ddcapr(t).*sarr(t) + 2*dcapr(t).*dsarr(t) + capr(t).*ddsarr(t));
z_RL3 = @(t) -capr(t).*carr(t)*com_RL3(1) - sapr(t).*carr(t)*com_RL3(2) + shrr(t)*com_RL3(3) + FootHeight + TibiaLength*capr(t).*carr(t);
dz_RL3 = @(t) -(dcapr(t).*carr(t) + capr(t).*dcarr(t))*com_RL3(1) - (dsapr(t).*carr(t) + sapr(t).*dcarr(t))*com_RL3(2) + dshrr(t)*com_RL3(3) + TibiaLength*(dcapr(t).*carr(t) + capr(t).*dcarr(t));
ddz_RL3 = @(t) -(ddcapr(t).*carr(t) + 2*dcapr(t).*dcarr(t) + capr(t).*ddcarr(t))*com_RL3(1) - (ddsapr(t).*carr(t) + 2*dsapr(t).*dcarr(t) + sapr(t).*ddcarr(t))*com_RL3(2) + ddshrr(t)*com_RL3(3) + TibiaLength*(ddcapr(t).*carr(t) + 2*dcapr(t).*dcarr(t) + capr(t).*ddcarr(t));

x1 = @(t) x1(t) + m_RL3 * (ddz_RL3(t) + g) .* x_RL3(t);
x2 = @(t) x2(t) + m_RL3 * ddx_RL3(t) .* z_RL3(t);
x3 = @(t) x3(t) + m_RL3 * (ddz_RL3(t) + g);
y1 = @(t) y1(t) + m_RL3 * (ddy_RL3(t) + g) .* y_RL3(t);
y2 = @(t) y2(t) + m_RL3 * ddy_RL3(t) .* z_RL3(t);
y3 = @(t) y3(t) + m_RL3 * (ddz_RL3(t) + g);
KE = @(t) KE(t) + m_RL3 * (dx_RL3(t).^2 + dy_RL3(t).^2 + dz_RL3(t).^2) / 2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Right Thigh (Frame RL2) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%
m_RL2 = 0.39421;
com_RL2 = [0.05352 0.00132 0.00235]';
% Get cooordinates and their derivatives with reference to the support foot.
% >> FK_sym; simplify((T_Torso_RL0*T_L01*T_L12*T_L23*T_L34*T_L45*T_L5E)^-1) % using right leg angles.
% [          -sin(h_p),          -cos(h_p),        0,                                                                            TibiaLength*sin(h_p + k_p) + ThighLength*sin(h_p)]
% [  cos(h_p)*sin(h_r), -sin(h_p)*sin(h_r), cos(a_r),                                                                -sin(h_r)*(TibiaLength*cos(h_p + k_p) + ThighLength*cos(h_p))]
% [ -cos(h_p)*cos(h_r),  cos(h_r)*sin(h_p), sin(h_r), FootHeight + TibiaLength*cos(a_p)*cos(a_r) + ThighLength*cos(a_p)*cos(a_r)*cos(k_p) - ThighLength*cos(a_r)*sin(a_p)*sin(k_p)]
% [                  0,                  0,        0,                                                                                                                            1]
x_RL2 = @(t) -shpr(t)*com_RL2(1) - chpr(t)*com_RL2(2) - TibiaLength*sapr(t) + ThighLength*shpr(t);
dx_RL2 = @(t) -dshpr(t)*com_RL2(1) - dchpr(t)*com_RL2(2) - TibiaLength*dsapr(t) + ThighLength*dshpr(t);
ddx_RL2 = @(t) -ddshpr(t)*com_RL2(1) - ddchpr(t)*com_RL2(2) - TibiaLength*ddsapr(t) + ThighLength*ddshpr(t);
y_RL2 = @(t) chpr(t).*shrr(t)*com_RL2(1) - shpr(t).*shrr(t)*com_RL2(2) + carr(t)*com_RL2(3) - TibiaLength*capr(t).*shrr(t) - ThighLength*chpr(t).*shrr(t);
dy_RL2 = @(t) (dchpr(t).*shrr(t) + chpr(t).*dshrr(t))*com_RL2(1) - (dshpr(t).*shrr(t) + shpr(t).*dshrr(t))*com_RL2(2) + dcarr(t)*com_RL2(3) - TibiaLength*(dcapr(t).*shrr(t) + capr(t).*dshrr(t)) - ThighLength*(dchpr(t).*shrr(t) + chpr(t).*dshrr(t));
ddy_RL2 = @(t) (ddchpr(t).*shrr(t) + 2*dchpr(t).*dshrr(t) + chpr(t).*ddshrr(t))*com_RL2(1) - (ddshpr(t).*shrr(t) + 2*dshpr(t).*dshrr(t) + shpr(t).*ddshrr(t))*com_RL2(2) + ddcarr(t)*com_RL2(3) - TibiaLength*(ddcapr(t).*shrr(t) + 2*dcapr(t).*dshrr(t) + capr(t).*ddshrr(t)) - ThighLength*(ddchpr(t).*shrr(t) + 2*dchpr(t).*dshrr(t) + chpr(t).*ddshrr(t));
z_RL2 = @(t) -chpr(t).*chrr(t)*com_RL2(1) + shpr(t).*chrr(t)*com_RL2(2) + shrr(t)*com_RL2(3) + FootHeight + TibiaLength*capr(t).*carr(t) + ThighLength*carr(t).*chpr(t);
dz_RL2 = @(t) -(dchpr(t).*chrr(t) + chpr(t).*dchrr(t))*com_RL2(1) - (dshpr(t).*chrr(t) + shpr(t).*dchrr(t))*com_RL2(2) + dshrr(t)*com_RL2(3) + TibiaLength*(dcapr(t).*carr(t) + capr(t).*dcarr(t)) + ThighLength*(dcarr(t).*chpr(t) + carr(t).*dchpr(t));
ddz_RL2 = @(t) -(ddchpr(t).*chrr(t) + 2*dchpr(t).*dchrr(t) + chpr(t).*ddchrr(t))*com_RL2(1) - (ddshpr(t).*chrr(t) + 2*dshpr(t).*dchrr(t) + shpr(t).*ddcarr(t))*com_RL2(2) + ddshrr(t)*com_RL2(3) + TibiaLength*(ddcapr(t).*carr(t) + 2*dcapr(t).*dcarr(t) + capr(t).*ddcarr(t)) + ThighLength*(ddcarr(t).*chpr(t) + 2*dcarr(t).*dchpr(t) + carr(t).*ddchpr(t));

x1 = @(t) x1(t) + m_RL2 * (ddz_RL2(t) + g) .* x_RL2(t);
x2 = @(t) x2(t) + m_RL2 * ddx_RL2(t) .* z_RL2(t);
x3 = @(t) x3(t) + m_RL2 * (ddz_RL2(t) + g);
y1 = @(t) y1(t) + m_RL2 * (ddy_RL2(t) + g) .* y_RL2(t);
y2 = @(t) y2(t) + m_RL2 * ddy_RL2(t) .* z_RL2(t);
y3 = @(t) y3(t) + m_RL2 * (ddz_RL2(t) + g);
KE = @(t) KE(t) + m_RL2 * (dx_RL2(t).^2 + dy_RL2(t).^2 + dz_RL2(t).^2) / 2;

x_zmp = @(t) (x1(t) - x2(t)) ./ x3(t);
y_zmp = @(t) (y1(t) - y2(t)) ./ y3(t);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Force Propagation (unused because it's very slow..) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fx_LLE = @(t) m_LLE * ddx_LLE(t);
fy_LLE = @(t) m_LLE * ddy_LLE(t);
fz_LLE = @(t) m_LLE * (ddz_LLE(t) - g);

%>> FK_sym; R(5,6)
% [  sin(a_p_l), -cos(a_p_l)*sin(a_r_l), -cos(a_p_l)*cos(a_r_l)]
% [ -cos(a_p_l), -sin(a_p_l)*sin(a_r_l), -cos(a_r_l)*sin(a_p_l)]
% [           0,             cos(a_r_l),             sin(h_r_l)]
fx_LL3 = @(t)             sapl(t).*fx_LLE(t)   -capl(t).*sarl(t).*fy_LLE(t)   -capl(t).*carl(t).*fz_LLE(t) + m_LL3*ddx_LL3(t) -capl(t).*carl(t)*m_LL3*g;
fy_LL3 = @(t)            -capl(t).*fx_LLE(t)   -sapl(t).*sarl(t).*fy_LLE(t)   -sapl(t).*carl(t).*fz_LLE(t) + m_LL3*ddy_LL3(t) -sapl(t).*carl(t)*m_LL3*g;
fz_LL3 = @(t)                                            carl(t).*fy_LLE(t)            +shrl(t).*fz_LLE(t) + m_LL3*ddz_LL3(t)          +shrl(t)*m_LL3*g;

%>> FK_sym; R(4,5)
% [ cos(k_p_l), -sin(k_p_l), 0]
% [ sin(k_p_l),  cos(k_p_l), 0]
% [          0,           0, 1]
fx_LL2 = @(t)             ckpl(t).*fx_LL3(t)            -skpl(t).*fy_LL3(t)                                + m_LL2*ddx_LL2(t) -chpl(t).*chrl(t)*m_LL2*g;
fy_LL2 = @(t)             skpl(t).*fx_LL3(t)            +ckpl(t).*fy_LL3(t)                                + m_LL2*ddy_LL2(t) +shpl(t).*chrl(t)*m_LL2*g;
fz_LL2 = @(t)                                                                                    fz_LL3(t) + m_LL2*ddz_LL2(t)          +shrl(t)*m_LL2*g;

%>> FK_sym; R(3,4)
% [            -sin(h_p_l),            -cos(h_p_l),          0]
% [  cos(h_p_l)*sin(h_r_l), -sin(h_p_l)*sin(h_r_l), cos(h_r_l)]
% [ -cos(h_p_l)*cos(h_r_l),  cos(h_r_l)*sin(h_p_l), sin(h_r_l)]
fx_Torso = @(t)          -shpl(t).*fx_LL2(t)            -chpl(t).*fy_LL2(t)                                + m_Torso*ddx_Torso(t);
fy_Torso = @(t)  chpl(t).*shrl(t).*fx_LL2(t)   -shpl(t).*shrl(t).*fy_LL2(t)            +chrl(t).*fz_LL2(t) + m_Torso*ddy_Torso(t);
fz_Torso = @(t) -chpl(t).*chrl(t).*fx_LL2(t)   +shpl(t).*chrl(t).*fy_LL2(t)            +shrl(t).*fz_LL2(t) + m_Torso*(ddz_Torso(t) - g);

%>> FK_sym; R(2,3)
% [ -sin(h_p_r),  cos(h_p_r)*sin(h_r_r), -cos(h_p_r)*cos(h_r_r)]
% [ -cos(h_p_r), -sin(h_p_r)*sin(h_r_r),  cos(h_r_r)*sin(h_p_r)]
% [           0,             cos(h_r_r),             sin(h_r_r)]
fx_RL2 = @(t)          -shpr(t).*fx_Torso(t) +chpr(t).*shrr(t).*fy_Torso(t) -chpr(t).*chrr(t).*fz_Torso(t) + m_RL2*ddx_RL2(t) -chpr(t).*chrr(t)*m_RL2*g;
fy_RL2 = @(t)          -chpr(t).*fx_Torso(t) -shpr(t).*shrr(t).*fy_Torso(t) +shpr(t).*chrr(t).*fz_Torso(t) + m_RL2*ddy_RL2(t) +shpr(t).*chrr(t)*m_RL2*g;
fz_RL2 = @(t)                                         +chrr(t).*fy_Torso(t)          +shrr(t).*fz_Torso(t) + m_RL2*ddz_RL2(t)          +shrr(t)*m_RL2*g;

%>> FK_sym; R(1,2)
% [  cos(k_p_r), sin(k_p_r), 0]
% [ -sin(k_p_r), cos(k_p_r), 0]
% [           0,          0, 1]
fx_RL3 = @(t)             ckpr(t).*fx_RL2(t)            +skpr(t).*fy_RL2(t)                                + m_RL3*ddx_RL3(t) -capr(t).*carr(t)*m_RL3*g;
fy_RL3 = @(t)            -skpr(t).*fx_RL2(t)            +ckpr(t).*fy_RL2(t)                                + m_RL3*ddy_RL3(t) -sapr(t).*carr(t)*m_RL3*g;
fz_RL3 = @(t)                                                                                    fz_RL2(t) + m_RL3*ddz_RL3(t)          +shrr(t)*m_RL3*g;

%>> FK_sym; R(0,1)
% [             sin(a_p_r),            -cos(a_p_r),          0]
% [ -cos(a_p_r)*sin(a_r_r), -sin(a_p_r)*sin(a_r_r), cos(a_r_r)]
% [ -cos(a_p_r)*cos(a_r_r), -cos(a_r_r)*sin(a_p_r), sin(h_r_r)]
fx_RLE = @(t)             sapr(t).*fx_RL3(t)            -capr(t).*fy_RL3(t)                                + m_RLE*ddx_RLE(t);
fy_RLE = @(t)   -capr(t).*sarr(t).*fx_RL3(t)   -sapr(t).*sarr(t).*fy_RL3(t)            +carr(t).*fz_RL3(t) + m_RLE*ddy_RLE(t);
fz_RLE = @(t)                                                                                    fz_RL3(t) + m_RLE*(ddz_RLE(t) - g);

end

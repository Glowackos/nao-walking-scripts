function [a_r, a_p, k_p, h_r, h_p, ...
    da_r, da_p, dk_p, dh_r, dh_p, ...
    dda_r, dda_p, ddk_p, ddh_r, ddh_p] = IK(x, dx, ddx, y, dy, ddy, z, dz, ddz)
%IK Returns leg joint angles required to achieve a chosen R3 foot position relative to COM.
%   In order for the solution to be fully determined, some heuristics are
%   enforced:
%    - The robot torso must always remain vertical.
%    - The robot feet must always be parallel to the ground.

ThighLength = 0.1;
TibiaLength = 0.1029;
FootHeight = 0.04519;

Z = @(t) z(t) + FootHeight;
yzs = @(t) y(t).^2 + Z(t).^2;
xyzs = @(t) x(t).^2 + y(t).^2 + Z(t).^2;
xyzds = @(t) dx(t).^2 + dy(t).^2 + dz(t).^2;
xdx = @(t) x(t).*dx(t);
xddx = @(t) x(t).*ddx(t);
ydy = @(t) y(t).*dy(t);
yddy = @(t) y(t).*ddy(t);
zdz = @(t) Z(t).*dz(t);
zddz = @(t) Z(t).*ddz(t);


a_r = @(t) atan2(-y(t), -Z(t));
h_r = @(t) -a_r(t);

A = TibiaLength^2 + ThighLength^2;
B = 2*TibiaLength*ThighLength;

% This expression is used in an acos, so it cannot be greater than 1.
minxyzs = @(t) min((xyzs(t) - A) / B, 1);
% Knee pitch has negligible allowed motion in -ve direction, so exclude.
k_p = @(t) acos(minxyzs(t));

C = @(t) ThighLength*sin(k_p(t));
D = @(t) ThighLength*cos(k_p(t)) + TibiaLength;
E = @(t) sin(h_r(t));
CE = @(t) C(t)./E(t);
DE = @(t) D(t)./E(t);

u = @(t) D(t).*x(t) - CE(t).*y(t);
v = @(t) C(t).*x(t) + DE(t).*y(t);

a_p = @(t) atan2(u(t), v(t));
h_p = @(t) -a_p(t) - k_p(t);

% Derivatives
da_r = @(t) (dy(t).*Z(t) - dz(t).*y(t)) ./ yzs(t);
dda_r = @(t) (ddy(t).*Z(t) - ddz(t).*y(t) - 2*da_r(t).*(ydy(t)+zdz(t))) ./ yzs(t);
dh_r = @(t) -da_r(t);
ddh_r = @(t) -dda_r(t);

% This expression is used both in a sqrt and as a divisor, so it must be
% strictly positive to avoid complex/undefined output.
% Values close to zero will result in huge joint angle velocities, however
% we expect these cases to get filtered out later in the scoring.
F = @(t) max(1-minxyzs(t).^2, 1e-5);
dk_p = @(t) -2*(xdx(t) + ydy(t) + zdz(t)) / B ./ sqrt(F(t));
ddk_p = @(t) 2*(xyzds(t) + xddx(t) + yddy(t) + zddz(t))./(B*sqrt(F(t))) - 4*(xdx(t) + ydy(t) + zdz(t)).^2.*(xyzs(t)-A)./(B^3*F(t).^(3/2));

dC = @(t) ThighLength*cos(k_p(t)).*dk_p(t);
ddC = @(t) ThighLength*(cos(k_p(t)).*ddk_p(t) - sin(k_p(t)).*dk_p(t).^2);
dD = @(t) -ThighLength*sin(k_p(t)).*dk_p(t);
ddD = @(t) -ThighLength*(sin(k_p(t)).*ddk_p(t) + cos(k_p(t)).*dk_p(t).^2);
dE = @(t) cos(h_r(t)).*dh_r(t);
ddE = @(t) cos(h_r(t)).*ddh_r(t) - sin(h_r(t)).*dh_r(t);
dCE = @(t) (dC(t).*E(t) - dE(t).*C(t))./E(t).^2;
ddCE = @(t) (ddC(t).*E(t) - ddE(t).*C(t) - 2*dCE(t).*E(t).*dE(t)) ./ E(t).^2;
dDE = @(t) (dD(t).*E(t) - dE(t).*D(t))./E(t).^2;
ddDE = @(t) (ddD(t).*E(t) - ddE(t).*D(t) - 2*dDE(t).*E(t).*dE(t)) ./ E(t).^2;


du = @(t) dD(t).*x(t) + D(t).*dx(t) - dCE(t).*y(t) - CE(t).*dy(t);
ddu = @(t) ddD(t).*x(t) + 2*dD(t).*dx(t) + D(t).*ddx(t) - ddCE(t).*y(t) - 2*dCE(t).*dy(t) - CE(t).*ddy(t);
dv = @(t) dC(t).*x(t) + C(t).*dx(t) + dDE(t).*y(t) + DE(t).*dy(t);
ddv = @(t) ddC(t).*x(t) + 2*dC(t).*dx(t) + C(t).*ddx(t) + ddDE(t).*y(t) + 2*dDE(t).*dy(t) + DE(t).*ddy(t);

da_p = @(t) (du(t).*v(t)-dv(t).*u(t))./(u(t).^2+v(t).^2);
dda_p = @(t) (ddu(t).*v(t) - ddv(t).*u(t) - 2*da_p(t).*(u(t).*du(t)+v(t).*dv(t))) ./ (u(t).^2 + v(t).^2);
dh_p = @(t) -da_p(t) - dk_p(t);
ddh_p = @(t) -dda_p(t) - ddk_p(t);

end

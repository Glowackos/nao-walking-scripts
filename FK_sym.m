% Transformation matrix from Denavit-Hartenberg parameters
% a & alpha are distance along and rotation about current x-axis.
% d & theta are distance along and rotation about new z-axis.
T  = @(a, alpha, d, theta) [           cos(theta)           -sin(theta)           0             a;
                            sin(theta)*cos(alpha) cos(theta)*cos(alpha) -sin(alpha) -d*sin(alpha);
                            sin(theta)*sin(alpha) cos(theta)*sin(alpha)  cos(alpha)  d*cos(alpha);
                                                0                     0           0             1];

syms a_r a_p k_p h_r h_p pi
% Let matlab know of our assumptions so that it can simplify better.
assume(a_p + k_p + h_p == 0)
assume(a_r + h_r == 0)


syms HipOffsetY HipOffsetZ ThighLength TibiaLength FootHeight

% Torso to left hip (L0 means leg 0)
T_Torso_LL0 = [1 0 0           0;
               0 1 0  HipOffsetY;
               0 0 1 -HipOffsetZ;
               0 0 0           1];

% Torso to right hip (L0 means leg 0)
T_Torso_RL0 = [1 0 0           0;
               0 1 0 -HipOffsetY;
               0 0 1 -HipOffsetZ;
               0 0 0           1];

% The remaining transformations are common to both legs.
% The first and last transformations include additional steps because we
% desire L0 and LE to have their axes oriented the same way as the torso.
T_L01 = [ 0 0 1 0;
          0 1 0 0;
         -1 0 0 0;
          0 0 0 1] * ...
        T(          0,     0, 0, h_r);
T_L12 = T(          0, -pi/2, 0, h_p);
T_L23 = T(ThighLength,     0, 0, k_p);
T_L34 = T(TibiaLength,     0, 0, a_p);
T_L45 = T(          0,  pi/2, 0, a_r);
T_L5E = T( FootHeight,     0, 0,   0) * ...
        [0 0 -1 0;
         0 1  0 0;
         1 0  0 0;
         0 0  0 1];


% Torse to left leg end effector.
T_Torso_LLE = T_Torso_LL0*T_L01*T_L12*T_L23*T_L34*T_L45*T_L5E;
% Torse to right leg end effector.
T_Torso_RLE = T_Torso_RL0*T_L01*T_L12*T_L23*T_L34*T_L45*T_L5E;


%%%%%%%%%%%%
% Dynamics %
%%%%%%%%%%%%
% 0 - RLE
% 1 - RL3
% 2 - RL2
% 3 - Torso
% 4 - LL2
% 5 - LL3
% 6 - LLE

n = 6;

syms a_r_r a_p_r k_p_r h_r_r h_p_r a_r_l a_p_l k_p_l h_r_l h_p_l
subsr = @(f) subs(f, {a_r a_p k_p h_r h_p}, {a_r_r a_p_r k_p_r h_r_r h_p_r});
subsl = @(f) subs(f, {a_r a_p k_p h_r h_p}, {a_r_l a_p_l k_p_l h_r_l h_p_l});

assume(a_p_r + k_p_r + h_p_r == 0)
assume(a_r_r + h_r_r == 0)
assume(a_p_l + k_p_l + h_p_l == 0)
assume(a_r_l + h_r_l == 0)

T = sym(zeros(4,4,n));
% T01 (T_RLE3)
T(:,:,1) = subsr((T_L34*T_L45*T_L5E)^-1);
% T12 (T_RL32)
T(:,:,2) = subsr(T_L23^-1);
% T23 (T_RL2_Torso)
T(:,:,3) = subsr((T_Torso_RL0*T_L01*T_L12)^-1);
% T34 (T_Torso_LL2)
T(:,:,4) = subsl(T_Torso_LL0*T_L01*T_L12);
% T45 (T_LL23)
T(:,:,5) = subsl(T_L23);
% T56 (T_LL3E)
T(:,:,6) = subsl(T_L34*T_L45*T_L5E);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Recursion with anonymous functions! :D %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
syms m_RLE m_RL3 m_RL2 m_Torso m_LL2 m_LL3 m_LLE g
m_ = {m_RLE m_RL3 m_RL2 m_Torso m_LL2 m_LL3 m_LLE};
m = @(i) m_{i+1};
%Pi (position component of T(i-1)i)
P = @(i) T(1:3,4,i);
%Rij
R_ = @(i,j,b) b{(j-i==2) + 2}(i,j-1,b) * b{3}(j-1,j,b);
R_branches = {@(i,j,b) b{(i-j==1) + 2}(j,i,b).' R_ @(i,j,b) T(1:3,1:3,j) @(i,j,b) sym(eye(3))};
R = @(i,j) simplify(R_branches{(i<j) + (j-i==1) + 3*(i==j) + 1}(i,j,R_branches));
%f_i (measured in own frame)
f_ = @(i,b) R(i,i+1)*b{(i<n-1)+1}(i+1,b) + R(i,3)*[sym(0) sym(0) m(i)*g].';
f_branches = {@(i,b) [sym(0) sym(0) m(n)*g].' f_};
f = @(i) simplify(f_branches{(i<n)+1}(i,f_branches));
%n_i (measured in own frame)
n_ = @(i,b) R(i,i+1)*(b{(i<n-1)+1}(i+1,b) + cross(P(i+1),f(i)));
n_branches = {@(i,b) sym(zeros(3,1)) n_};
n = @(i) simplify(n_branches{(i<n)+1}(i,n_branches));

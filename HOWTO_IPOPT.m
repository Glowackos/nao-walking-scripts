% USING OPTI_TOOL_SOLVER
close all
% 1. Define objective function (and its derivative).
f = @(x,y) x.^2.*(x-2).^2 + y.^2 + (x-2)*y;
df = @(x,y) [4*x.^3 - 12*x.^2 + 8*x + x*y - 2*y, 2*y + x - 2];
[X,Y] = meshgrid(-1.5:0.01:3);
mesh(X,Y,f(X,Y))
% 2. Define wrapper function for it that takes just one vector parameter.
fun = @(x) f(x(1),x(2));
dfun = @(x) df(x(1),x(2));
% 3. Make a starting guess.
x0 = [0,0];
% 4. Define options
% Here we choose IPOPT solver and elect to have output displayed at
% every iteration.
options = optiset('solver','ipopt','display','iter');
% 5. Declare the opti object, providing our function and options.
Opt = opti('fun',fun,'grad',dfun,'x0',x0,'options',options);
% 6. Solve!
[x, fval, exitflag, output] = solve(Opt);

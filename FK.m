function [T_Torso_LL0, T_Torso_RL0, ...
    T_L01, T_L12, T_L23, T_L34, T_L45, T_L5E] = ...
    FK(a_r, a_p, k_p, h_r, h_p)
%FK Returns leg transformations matrices from given leg joint angles.

% Transformation matrix from Denavit-Hartenberg parameters
% a & alpha are distance along and rotation about current x-axis.
% d & theta are distance along and rotation about new z-axis.
T  = @(a, alpha, d, theta) [           cos(theta)           -sin(theta)           0             a;
                            sin(theta)*cos(alpha) cos(theta)*cos(alpha) -sin(alpha) -d*sin(alpha);
                            sin(theta)*sin(alpha) cos(theta)*sin(alpha)  cos(alpha)  d*cos(alpha);
                                                0                     0           0             1];

HipOffsetY = 0.05;
HipOffsetZ = 0.085;
ThighLength = 0.1;
TibiaLength = 0.1029;
FootHeight = 0.04519;

% Torso to left hip (L0 means leg 0)
T_Torso_LL0 = [1 0 0           0;
               0 1 0  HipOffsetY;
               0 0 1 -HipOffsetZ;
               0 0 0           1];

% Torso to right hip (L0 means leg 0)
T_Torso_RL0 = [1 0 0           0;
               0 1 0 -HipOffsetY;
               0 0 1 -HipOffsetZ;
               0 0 0           1];

% The remaining transformations are common to both legs.
% The first and last transformations include additional steps because we
% desire L0 and LE to have their axes oriented the same way as the torso.
T_L01 = @(t) [ 0 0 1 0;
               0 1 0 0;
              -1 0 0 0;
               0 0 0 1] * ...
             T(          0,     0, 0, h_r(t));
T_L12 = @(t) T(          0, -pi/2, 0, h_p(t));
T_L23 = @(t) T(ThighLength,     0, 0, k_p(t));
T_L34 = @(t) T(TibiaLength,     0, 0, a_p(t));
T_L45 = @(t) T(          0,  pi/2, 0, a_r(t));
T_L5E = @(t) T( FootHeight,     0, 0,   0) * ...
             [0 0 -1 0;
              0 1  0 0;
              1 0  0 0;
              0 0  0 1];

%
% % Torse to left leg end effector.
% T_Torso_LLE = T_Torso_LL0*T_L01*T_L12*T_L23*T_L34*T_L45*T_L5E;
% % Torse to right leg end effector.
% T_Torso_RLE = T_Torso_RL0*T_L01*T_L12*T_L23*T_L34*T_L45*T_L5E;

end

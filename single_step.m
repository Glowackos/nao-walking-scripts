function [score, ...
    x_r, dx_r, ddx_r, y_r, dy_r, ddy_r, z_r, dz_r, ddz_r, ...
    x_l, dx_l, ddx_l, y_l, dy_l, ddy_l, z_l, dz_l, ddz_l, ...
    a_r_r, a_p_r, k_p_r, h_r_r, h_p_r, ...
    a_r_l, a_p_l, k_p_l, h_r_l, h_p_l, ...
    da_r_r, da_p_r, dk_p_r, dh_r_r, dh_p_r, ...
    da_r_l, da_p_l, dk_p_l, dh_r_l, dh_p_l, ...
    dda_r_r, dda_p_r, ddk_p_r, ddh_r_r, ddh_p_r, ...
    dda_r_l, dda_p_l, ddk_p_l, ddh_r_l, ddh_p_l, ...
    x_zmp, y_zmp, KE] = ...
    single_step(T, S, z_max, hip_height, foot_separation, K, SCORING_METHOD, VERBOSITY)
%SINGLE_STEP Generates a COM trajectory for a single step for the NAO
% robot using the Augmented Linear Inverted Pendulum (ALIP) model.
%Inverse kinematics are applied to the result to determine
% the angle trajectories to be sent to the robot.
%This is then scored based on velocity minimisation.
%
% INPUTS
% (Original values in square brackets)
% T = 0.3; % time to complete a single step [1.0]
% S = 0.12; % x-distance travelled per step when at constant pace [0.05]
% z_max = 0.03; % maximum height reached by swing leg [0.015]
%
% hip_height = 0.16; % [0.16]
% foot_separation = 0.08; % y-distance between centers of the feet [0.08]
%
% ALIP PARAMETERS
% Terms of the augmenting function for the x-position model.
% K(1) = 0; % x position coefficient
% K(2) = 0; % x velocity coefficient
% % Terms of the augmenting function for the y-position model.
% K(3) = 0; % y position coefficient
% K(4) = 0; % y velocity coefficient

% This value is a constant.
COM_HIP_OFFSET_Z = 0.12;

%% %%%%%%%%%%%%%%%%
% FOOT TRAJECTORY %
%%%%%%%%%%%%%%%%%%%
% Foot spends half of the step period as the support foot.
% We use ALIP here to determine the COM trajectory.
% The foot trajectory is just the negative of this, however we then need
% to shift the reference frame to be the hip joint so that we can apply
% the same IK function for both legs.
[x_com, dx_com, ddx_com, y_com, dy_com, ddy_com] = ALIP(T/2, S/2, foot_separation, hip_height+COM_HIP_OFFSET_Z, K);
% Foot spends other half of the step period as the swing foot.
[x_swing, dx_swing, ddx_swing, z_swing, dz_swing, ddz_swing] = swing_leg_traj(T/2, S/2, z_max);

% y solutions coming out of ALIP may not always be continuous in position.
% Adjusting them by half the difference between the start and end of the
% trajectory fixes this.
y_offset = (y_com(0) - y_com(T/2)) / 2;

% Designate right foot as the support foot for the first half of the step.
x_r = @(t) (t<=T/2) .* -x_com(t);
dx_r = @(t) (t<=T/2) .* -dx_com(t);
ddx_r = @(t) (t<=T/2) .* -ddx_com(t);
y_r = @(t) (t<=T/2) .* (-y_com(t) + foot_separation/2 - y_offset);
dy_r = @(t) (t<=T/2) .* -dy_com(t);
ddy_r = @(t) (t<=T/2) .* -ddy_com(t);
z_r = @(t) (t<=T/2) .* -hip_height;
dz_r = @(t) (t<=T/2) .* 0;
ddz_r = @(t) (t<=T/2) .* 0;
x_r = @(t) x_r(t) + (t>T/2) .* (x_swing((t-T/2).*(t>T/2)) - x_com(T/2));
dx_r = @(t) dx_r(t) + (t>T/2) .* dx_swing((t-T/2).*(t>T/2));
ddx_r = @(t) ddx_r(t) + (t>T/2) .* ddx_swing((t-T/2).*(t>T/2));
y_r = @(t) y_r(t) + (t>T/2) .* (y_com((t-T/2).*(t>T/2)) - foot_separation/2 + y_offset);
dy_r = @(t) dy_r(t) + (t>T/2) .* dy_com((t-T/2).*(t>T/2));
ddy_r = @(t) ddy_r(t) + (t>T/2) .* ddy_com((t-T/2).*(t>T/2));
z_r = @(t) z_r(t) + (t>T/2) .* (z_swing((t-T/2).*(t>T/2)) - hip_height);
dz_r = @(t) dz_r(t) + (t>T/2) .* dz_swing((t-T/2).*(t>T/2));
ddz_r = @(t) ddz_r(t) + (t>T/2) .* ddz_swing((t-T/2).*(t>T/2));

% Designate left foot as the swing foot for the first half of the step.
x_l = @(t) (t<=T/2) .* (x_swing(t) - x_com(T/2));
dx_l = @(t) (t<=T/2) .* dx_swing(t);
ddx_l = @(t) (t<=T/2) .* ddx_swing(t);
y_l = @(t) (t<=T/2) .* (foot_separation/2 - y_com(t) - y_offset);
dy_l = @(t) (t<=T/2) .* -dy_com(t);
ddy_l = @(t) (t<=T/2) .* -ddy_com(t);
z_l = @(t) (t<=T/2) .* (z_swing(t) - hip_height);
dz_l = @(t) (t<=T/2) .* dz_swing(t);
ddz_l = @(t) (t<=T/2) .* ddz_swing(t);
x_l = @(t) x_l(t) + (t>T/2) .* -x_com((t-T/2).*(t>T/2));
dx_l = @(t) dx_l(t) + (t>T/2) .* -dx_com((t-T/2).*(t>T/2));
ddx_l = @(t) ddx_l(t) + (t>T/2) .* -ddx_com((t-T/2).*(t>T/2));
y_l = @(t) y_l(t) + (t>T/2) .* (y_com((t-T/2).*(t>T/2)) - foot_separation/2 + y_offset);
dy_l = @(t) dy_l(t) + (t>T/2) .* dy_com((t-T/2).*(t>T/2));
ddy_l = @(t) ddy_l(t) + (t>T/2) .* ddy_com((t-T/2).*(t>T/2));
z_l = @(t) z_l(t) + (t>T/2) .* -hip_height;
dz_l = @(t) dz_l(t) + (t>T/2) .* 0;
ddz_l = @(t) ddz_l(t) + (t>T/2) .* 0;

%% %%%%%%%%%%%%%%%%%
% JOINT TRAJECTORY %
%%%%%%%%%%%%%%%%%%%%
% Determine angle trajectories from IK (computed once, in calling function).
[a_r_r, a_p_r, k_p_r, h_r_r, h_p_r, ...
 da_r_r, da_p_r, dk_p_r, dh_r_r, dh_p_r, ...
 dda_r_r, dda_p_r, ddk_p_r, ddh_r_r, ddh_p_r] = IK(x_r, dx_r, ddx_r, y_r, dy_r, ddy_r, z_r, dz_r, ddz_r);
[a_r_l, a_p_l, k_p_l, h_r_l, h_p_l, ...
 da_r_l, da_p_l, dk_p_l, dh_r_l, dh_p_l, ...
 dda_r_l, dda_p_l, ddk_p_l, ddh_r_l, ddh_p_l] = IK(x_l, dx_l, ddx_l, y_l, dy_l, ddy_l, z_l, dz_l, ddz_l);

%% %%%%%%
% SCORE %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Scores the strength of the trajectory based on the KE req'd. %
% The lower the score, the better.                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[x_zmp, y_zmp, KE] = ZMP(...
    a_r_r, a_p_r, k_p_r, h_r_r, h_p_r, ...
    da_r_r, da_p_r, dk_p_r, dh_r_r, dh_p_r, ...
    dda_r_r, dda_p_r, ddk_p_r, ddh_r_r, ddh_p_r, ...
    a_r_l, a_p_l, k_p_l, h_r_l, h_p_l, ...
    da_r_l, da_p_l, dk_p_l, dh_r_l, dh_p_l, ...
    dda_r_l, dda_p_l, ddk_p_l, ddh_r_l, ddh_p_l);

if SCORING_METHOD == 0
    score = 0;
    return
end
% The smaller the precision value, the better the precision, though
% computation time will be slower.
precision = 0.01;
t_disc = precision:precision:T/2-precision;
zmp_score = @(t) x_zmp(t).^2 + y_zmp(t).^2;
KE_score = @(t) KE(t);
penalty = @(x, o) exp(1e3*(x - o));
KE_score_with_ZMP_penalty = @(t) KE_score(t) + ...
    penalty(-x_zmp(t),0.03) + penalty(x_zmp(t),0.07) + ...
    penalty(-y_zmp(t),0.03) + penalty(y_zmp(t),0.02);
scoring_methods = {zmp_score KE_score KE_score_with_ZMP_penalty};
score = trapz(scoring_methods{SCORING_METHOD}(t_disc)) / precision;
if VERBOSITY == 2
    fprintf('[')
    fprintf('%i, ', K)
    fprintf('], score = %.8f\n', score)
end

end

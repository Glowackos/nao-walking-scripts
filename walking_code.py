from copy import copy
from exceptions import RuntimeError
import sys

import numpy as np
import scipy.io

from naoqi import ALProxy

ip = 'localhost'
try:
    filename = sys.argv[1]
except IndexError:
    filename = 'joint_trajectories.mat'


NUM_STEPS = 12

JOINT_NAMES = {
    'a_r_r': 'RAnkleRoll',
    'a_p_r': 'RAnklePitch',
    'k_p_r': 'RKneePitch',
    'h_r_r': 'RHipRoll',
    'h_p_r': 'RHipPitch',
    'a_r_l': 'LAnkleRoll',
    'a_p_l': 'LAnklePitch',
    'k_p_l': 'LKneePitch',
    'h_r_l': 'LHipRoll',
    'h_p_l': 'LHipPitch',
}
if '--arms' in sys.argv:
    JOINT_NAMES.update({
        's_p_r': 'RShoulderPitch',
        's_p_l': 'LShoulderPitch',
    })

print("Connecting...")
# memoryProxy = ALProxy("ALMemory", ip, 9559)
motion = ALProxy("ALMotion", ip, 9559)
posture = ALProxy("ALRobotPosture", ip, 9559)
# fsrproxy = ALProxy("ALFsr", ip, 9559)

motion.setStiffnesses("Body", 1)

print("Loading walking model...")
joint_trajectories = scipy.io.loadmat(filename, struct_as_record=False)
period = joint_trajectories['T'].reshape(-1)[0]
step = joint_trajectories['delta_t'].reshape(-1)[0]
timeline = [list(np.arange(step, period * NUM_STEPS + step, step))] * len(JOINT_NAMES)

# Inject shoulder pitch motion (copy hip and ankle roll angles)
ARM_SWING_FACTOR = 2
# For some annoying reason the shoulder motions hit the velocity restrictions.
# TODO: figure out why this is happening and then replace the 'while' with a
# better solution, removing all these 'PI_FACTOR' shenanigans.
PI_FACTOR = 2
temp_trajectories = [copy(joint_trajectories['a_r_r']), copy(joint_trajectories['h_r_r'])]
while True:
    joint_trajectories['s_p_r'] = temp_trajectories[0] * ARM_SWING_FACTOR + np.pi/PI_FACTOR
    joint_trajectories['s_p_l'] = temp_trajectories[1] * ARM_SWING_FACTOR + np.pi/PI_FACTOR
    if '--nohips' in sys.argv:
        joint_trajectories['a_r_r'] *= 0
        joint_trajectories['a_r_l'] *= 0
        joint_trajectories['h_r_r'] *= 0
        joint_trajectories['h_r_l'] *= 0

    angles = [list(np.array(joint_trajectories[joint]).reshape(-1)) * NUM_STEPS for joint in JOINT_NAMES.keys()]
    print("Walking code started...")
    # Assume starting position. Start with standing posture to drop arms to the robot's side.
    posture.goToPosture("Stand", 0.8)  # Comment out this line to walk like zombie.
    motion.angleInterpolationWithSpeed(JOINT_NAMES.values(), [a[0] for a in angles], 1.0)
    # Walk!
    try:
        motion.angleInterpolation(JOINT_NAMES.values(), angles, timeline, True)
    except RuntimeError as e:
        if 'Max Velocity is not respected.' in str(e) and 'Shoulder' in str(e):
            # For some reason this offset causes trouble... have to reduce it.
            PI_FACTOR += 0.1
            # Increase amount arms swing to try to compensate..
            #ARM_SWING_FACTOR += 0.5
            print("Shoulder velocity limit hit, reducing starting angle...")
            continue
        raise
    break
# Return to standing posture (straighten legs).
posture.goToPosture("Stand", 0.8)

print("Exited Successfully")

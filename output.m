%% %%%%%%%%%%%%%%%%%
% INPUT PARAMETERS %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Original values in square brackets %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Optimisation:
% 0: none
% 1: x only
% 2: y only
% 3: x and y
OPTIMISATION = 0;

% Scoring method:
% 0: no scoring. Use only when optimisation set to none.
% 1: zmp scoring. i.e integrating over x_zmp(t)^2 + y_zmp(t)^2
% 2: KE scoring. i.e integrating over kinetic energy.
% 3: KE scoring with ZMP penalty. i.e minimising KE, while staying within support region.
SCORING_METHOD = 3;

% Verbosity:
% 0: no output to console
% 1: solver output only (every 10 steps)
% 2: solver output and warnings; trajectory generation score and K values
VERBOSITY = 2;

% Result plotting:
% 0: no graphs
% 1: task space and joint space graphs only
% 2: task space, joint space, and zmp/KE graphs
PLOTTING = 2;

% Show plot titles (plots for reports should omit titles as the plots are
% described in the caption).
% 0: plot titles disabled
% 1: plot titles enabled
SHOW_PLOT_TITLES = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% WITH HIP HEIGHT = 0.18 %
%%%%%%%%%%%%%%%%%%%%%%%%%%
% 0) LIPM
K = [0 0 0 0];
% 1) ALIP, zmp minimised
K = [1.131450e+01, 4.495360e+00, 5.755087e+01, -1.692201e-01, ];
% 2) [FAIL] ALIP, KE minimised
% K = [-2.088406e+01, 8.497033e+00, -3.290331e+01, 7.008561e+01, ];
% 3) [FAIL] ALIP, KE minimised in X only
% K = [-2.023430e+01, 8.472900e+00, 0, 0, ];
% 4) [REJECT] ALIP, zmp minimised in Y only
% K = [0, 0, 5.957939e+01, -4.443537e-02, ];
% 5) [FAIL] ALIP, zmp minimised in Y, KE minimised in X
% K = [-2.023430e+01, 8.472900e+00, 5.957939e+01, -4.443537e-02, ];
% 6) ALIP, KE minimised with ZMP penalisation (old, choppy implementation)
K = [1.124902e+01, 4.414029e+00, 5.716000e+01, 3.600968e+00, ];
% 7) ALIP, KE minimised with ZMP penalisation (new, smoother implementation)
K = [-8.946463e+00, 2.892845e+00, 3.078810e+01, -5.688173e-01, ];

%%%%%%%%%%%%%%%%%%%%%%%%%%
% WITH HIP HEIGHT = 0.24 %
%%%%%%%%%%%%%%%%%%%%%%%%%%
% 0) LIPM
K = [0 0 0 0];
% 1) ALIP, zmp minimised
K = [1.487598e+01, 2.355168e+00, 4.717835e+01, -2.257239e-01, ];
% 6) ALIP, KE minimised with ZMP penalisation (old, choppy implementation)
K = [1.477064e+01, 2.255768e+00, 4.660936e+01, 3.703492e+00, ];
% 7) ALIP, KE minimised with ZMP penalisation (new, smoother implementation)
K = [-6.518160e+00, 2.236639e+00, 2.462240e+01, -3.789582e-01, ];

T = 1.0; % time to complete a full step (both support and swing phase)[1.0]
S = 0.12; % x-distance travelled per step when at constant pace [0.05]
z_max = 0.02; % maximum height reached by swing leg [0.015]

hip_height = 0.24; % [0.18]
foot_separation = 0.08; % y-distance between centers of the feet [0.08]

cp_steps = 30; % num steps walking at constant pace period [20]
ac_dc_steps = 2; % num steps accelerating from stationary to constant pace [2]

delta_t = 0.005; % time increment between trajectory updates [0.01]

%% %%%%%%%%%%%%%%%%%%%%%%
% TRAJECTORY GENERATION %
%%%%%%%%%%%%%%%%%%%%%%%%%
if OPTIMISATION > 0
    % Define options
    if VERBOSITY == 0
        options = optiset('solver','ipopt', 'warnings', 'none');
    elseif VERBOSITY == 1
        options = optiset('solver','ipopt', 'display','iter');
    else
        options = optiset('solver','ipopt', 'display','iter', 'warnings', 'all');
    end
    % Define optimisation function for determining ALIP parameters.
    if OPTIMISATION == 1
        opt_fun = @(K_x) single_step(T, S, z_max, hip_height, foot_separation, [K_x(1) K_x(2) K(3) K(4)], SCORING_METHOD, VERBOSITY);
        % Set starting guess.
        K0_x = K(1:2);
        % Declare the opti object, providing function and options.
        Opt = opti('fun',opt_fun,'ineq',eye(2),[200 200],'x0',K0_x,'options',options);
        % Solve!
        [K_x, fval, exitflag, result] = solve(Opt);
        K(1:2) = K_x;
    elseif OPTIMISATION == 2
        opt_fun = @(K_y) single_step(T, S, z_max, hip_height, foot_separation, [K(1) K(2) K_y(1) K_y(2)], SCORING_METHOD, VERBOSITY);
        % Set starting guess.
        K0_y = K(3:4);
        % Declare the opti object, providing function and options.
        Opt = opti('fun',opt_fun,'ineq',eye(2),[200 200],'x0',K0_y,'options',options);
        % Solve!
        [K_y, fval, exitflag, result] = solve(Opt);
        K(3:4) = K_y;
    else
        opt_fun = @(K) single_step(T, S, z_max, hip_height, foot_separation, K, SCORING_METHOD, VERBOSITY);
        % Set starting guess.
        K0 = K;
        % Declare the opti object, providing function and options.
        Opt = opti('fun',opt_fun,'ineq',eye(4),[200 200 200 200],'x0',K0,'options',options);
        % Solve!
        [K, fval, exitflag, result] = solve(Opt);
    end
end

% Use our optimal K values to find trajectories.
[score, ...
    x_r, dx_r, ddx_r, y_r, dy_r, ddy_r, z_r, dz_r, ddz_r, ...
    x_l, dx_l, ddx_l, y_l, dy_l, ddy_l, z_l, dz_l, ddz_l, ...
    a_r_r, a_p_r, k_p_r, h_r_r, h_p_r, ...
    a_r_l, a_p_l, k_p_l, h_r_l, h_p_l, ...
    da_r_r, da_p_r, dk_p_r, dh_r_r, dh_p_r, ...
    da_r_l, da_p_l, dk_p_l, dh_r_l, dh_p_l, ...
    dda_r_r, dda_p_r, ddk_p_r, ddh_r_r, ddh_p_r, ...
    dda_r_l, dda_p_l, ddk_p_l, ddh_r_l, ddh_p_l, ...
    x_zmp, y_zmp, KE] = ...
    single_step(T, S, z_max, hip_height, foot_separation, K, SCORING_METHOD, VERBOSITY);


%% %%%%%%%%%%%%%%
% STORE RESULTS %
%%%%%%%%%%%%%%%%%
% Discretise joint angle trajectories so that we can graph and save them.
timeline = delta_t:delta_t:T;
disc = @(angle_fn) angle_fn(timeline);

% Store associated K values.
joint_trajectories.K = K;
% Store T and delta_T so that we can reconstruct timeline.
joint_trajectories.T = T;
joint_trajectories.delta_t = delta_t;
% Store joint angles.
joint_trajectories.a_r_r = disc(a_r_r);
joint_trajectories.a_p_r = disc(a_p_r);
joint_trajectories.k_p_r = disc(k_p_r);
joint_trajectories.h_r_r = disc(h_r_r);
joint_trajectories.h_p_r = disc(h_p_r);
joint_trajectories.a_r_l = disc(a_r_l);
joint_trajectories.a_p_l = disc(a_p_l);
joint_trajectories.k_p_l = disc(k_p_l);
joint_trajectories.h_r_l = disc(h_r_l);
joint_trajectories.h_p_l = disc(h_p_l);

% Save the joint trajectories to file, to be read by python script later.
save joint_trajectories.mat -struct joint_trajectories

%% %%%%%%%%%%
% PLOTTTING %
%%%%%%%%%%%%%
if PLOTTING == 0
    return
end
close all
FIGPOS = [0.15 0.15 0.7 0.7];

figure('name',sprintf('Task space trajectories of feet relative to hip joints. K = [%i, %i, %i, %i]', K), 'numbertitle','off', 'units', 'normalized', 'outerpos', FIGPOS)
subplot(3,3,1)
plot(timeline, x_l(timeline), '.', timeline, x_r(timeline), '.')
SHOW_PLOT_TITLES==1 && title('Position');
ylabel('x (m)')
xlim([0 T])
ylim([-S/2.5 S])
subplot(3,3,2)
plot(timeline, dx_l(timeline), '.', timeline, dx_r(timeline), '.')
SHOW_PLOT_TITLES==1 && title('Velocity');
ylabel('x (m/s)')
xlim([0 T])
ylim([-0.4 0.4])
subplot(3,3,3)
plot(timeline, ddx_l(timeline), '.', timeline, ddx_r(timeline), '.')
SHOW_PLOT_TITLES==1 && title('Acceleration');
ylabel('x (m/s^{2})')
xlim([0 T])
ylim([-2 2])
legend('left foot', 'right foot')
subplot(3,3,4)
plot(timeline(2:4:end), y_l(timeline(2:4:end)), '.', timeline(4:4:end), y_r(timeline(4:4:end)), '.')
ylabel('y (m)')
xlim([0 T])
ylim([-.04 .04])
subplot(3,3,5)
plot(timeline(2:4:end), dy_l(timeline(2:4:end)), '.', timeline(4:4:end), dy_r(timeline(4:4:end)), '.')
ylabel('y (m/s)')
xlim([0 T])
ylim([-0.4 0.4])
subplot(3,3,6)
plot(timeline(2:4:end), ddy_l(timeline(2:4:end)), '.', timeline(4:4:end), ddy_r(timeline(4:4:end)), '.')
ylabel('y (m/s^{2})')
xlim([0 T])
ylim([-4 4])
subplot(3,3,7)
plot(timeline, z_l(timeline), '.', timeline, z_r(timeline), '.')
xlabel('t (s)')
ylabel('z (m)')
xlim([0 T])
ylim([-hip_height -hip_height+z_max*1.5])
subplot(3,3,8)
plot(timeline, dz_l(timeline), '.', timeline, dz_r(timeline), '.')
xlabel('t (s)')
ylabel('z (m/s)')
xlim([0 T])
ylim([-0.2 0.2])
subplot(3,3,9)
plot(timeline, ddz_l(timeline), '.', timeline, ddz_r(timeline), '.')
xlabel('t (s)')
ylabel('z (m/s^{2})')
xlim([0 T])
ylim([-2 4])

figure('name',sprintf('Joint space trajectory of right foot. K = [%i, %i, %i, %i]', K), 'numbertitle','off', 'units', 'normalized', 'outerpos', FIGPOS)
subplot(1,3,1)
plot(timeline, disc(a_r_r), ...
     timeline, disc(a_p_r), ...
     timeline, disc(k_p_r), ...
     timeline, disc(h_r_r), ...
     timeline, disc(h_p_r), ...
     'LineWidth', 2)
SHOW_PLOT_TITLES==1 && title('Position');
xlabel('time (s)')
ylabel('position (rad)')
xlim([0 T])
ylim([-1.5 2])
subplot(1,3,2)
plot(timeline, disc(da_r_r), ...
     timeline, disc(da_p_r), ...
     timeline, disc(dk_p_r), ...
     timeline, disc(dh_r_r), ...
     timeline, disc(dh_p_r), ...
     'LineWidth', 2)
SHOW_PLOT_TITLES==1 && title('Velocity');
xlabel('time (s)')
ylabel('velocity (rad/s)')
xlim([0 T])
ylim([-3 3])
subplot(1,3,3)
plot(timeline, disc(dda_r_r), ...
     timeline, disc(dda_p_r), ...
     timeline, disc(ddk_p_r), ...
     timeline, disc(ddh_r_r), ...
     timeline, disc(ddh_p_r), ...
     'LineWidth', 2)
SHOW_PLOT_TITLES==1 && title('Acceleration');
xlabel('time (s)')
ylabel('acceleration (rad/s^{2})')
xlim([0 T])
ylim([-30 30])
legend('ankle roll','ankle pitch','knee pitch','hip roll','hip pitch')

if PLOTTING == 1
    return
end

figure('name',sprintf('ZMP Trajectory. K = [%i, %i, %i, %i]', K), 'numbertitle','off', 'units', 'normalized', 'outerpos', FIGPOS)
half_timeline = delta_t:delta_t:T/2-delta_t;
subplot(1,3,1)
hold on
plot(half_timeline, x_zmp(half_timeline), half_timeline, y_zmp(half_timeline))
plot(delta_t, x_zmp(delta_t), 'k.', delta_t, y_zmp(delta_t), 'k.')
hold off
SHOW_PLOT_TITLES==1 && title('ZMP over Half-Step');
xlabel('t (s)')
ylabel('position (m)')
legend('x', 'y')
xlim([0 T/2])
ylim([-0.1 0.06])
subplot(1,3,2)
rectangle('Position', [-0.03 -0.03 .05 .1], 'Curvature', 0.1)
hold on
plot(y_zmp(half_timeline), x_zmp(half_timeline))
plot(y_zmp(delta_t), x_zmp(delta_t), 'k.')
hold off
SHOW_PLOT_TITLES==1 && title('ZMP Polar Plot in Foot Region');
xlabel('y (m)')
ylabel('x (m)')
xlim([-0.04 0.06])
ylim([-0.1 0.08])
set(gca(),'xdir', 'reverse')
grid on
subplot(1,3,3)
hold on
plot(half_timeline, KE(half_timeline))
hold off
SHOW_PLOT_TITLES==1 && title('Kinetic Energy over Half-Step');
xlabel('t (s)')
ylabel('Energy (J)')
xlim([0 T/2])
ylim([0 0.35])

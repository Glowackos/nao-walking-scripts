function [score, ...
    timeline, total_timesteps, ...
    A_r_r, A_p_r, K_p_r, H_r_r, H_p_r, R_pos, ...
    A_r_l, A_p_l, K_p_l, H_r_l, H_p_l, L_pos, ...
    y_traj_s, y_traj_g, y_traj_zmp, v_traj, ...
    x_traj_store, x_traj_g_t, u_traj_store, ...
    x_support, y_support, x_zmp_g_store] = ...
gait_trajectory_generation(S, T, z_max, ...
    hip_height, h, foot_separation, ...
    cp_steps, ac_dc_steps, delta_t, ...
    k_v_x, k_p_x, k_v_y, k_p_y)
%GAIT_TRAJECTORY_GENERATION Generates a COM trajectory for the NAO robot
%using the Augmented Linear Inverted Pendulum (ALIP) model.
%Inverse kinematics are applied to the result to determine
% the angle trajectories to be sent to the robot.
%
% INPUTS
% (Original values in square brackets)
% S = 0.12; % x-distance travelled per step when at constant pace [0.05]
% T = 0.3; % time to complete a single step [1.0]
% z_max = 0.03; % maximum height reached by swing leg [0.015]
%
% hip_height = 0.16; % [0.16]
% h = 0.3; % height of centre of mass - varies with hip height [0.3]
% foot_separation = 0.08; % y-distance between centers of the feet [0.08]
%
% cp_steps = 10; % num steps walking at constant pace period [20]
% ac_dc_steps = 2; % num steps accelerating from stationary to constant pace [2]
%
% delta_t = 0.01; % time increment between trajectory updates [0.01]
%
% ALIP PARAMETERS
% Terms of the augmenting function for the x-position model.
% k_v_x = 0; % velocity coefficient
% k_p_x = 0; % position coefficient
% % Terms of the augmenting function for the y-position model.
% k_v_y = 0; % velocity coefficient
% k_p_y = 0; % position coefficient

%% %%%%%%%%%%%%%%%%%%%%%%
% SIMULATION PARAMETERS %
%%%%%%%%%%%%%%%%%%%%%%%%%
total_steps = cp_steps + 2*ac_dc_steps; % total number of steps

step_timeline = 0:delta_t:T-delta_t; % timeline of a single step
total_step_timesteps = length(step_timeline);

timeline = 0:delta_t:T*total_steps-delta_t; % timeline of entire walk
total_timesteps = length(timeline);

%% %%%%%%%%%%%%%%%%%%%%%%
% Y-TRAJECTORY SOLUTION %
%%%%%%%%%%%%%%%%%%%%%%%%%
[COM_Y, COM_Y_dot] = ALIP_Y(foot_separation, T, h, k_v_y, k_p_y);

% COM Y trajectory w.r.t current support foot.
y_traj_s = @(t) COM_Y(mod(t,T)) .* -sign(mod(t,2*T)-T);
% COM Y trajectory w.r.t ground.
y_traj_g = @(t) (COM_Y(mod(t,T)) - foot_separation/2) .* -sign(mod(t,2*T)-T);
% ZMP Y trajectory w.r.t ground.
y_traj_zmp = @(t) foot_separation/2 .* sign(mod(t,2*T)-T);
% COM Y velocity w.r.t ground.
v_traj = @(t) COM_Y_dot(mod(t,T)) .* -sign(mod(t,2*T)-T);

%% %%%%%%%%%%%%%%%%%%%%%%
% X-TRAJECTORY SOLUTION %
%%%%%%%%%%%%%%%%%%%%%%%%%
% Determine step size as a function of time.
% This is because there is an acceleration phase at the start and a
% deceleration phase at the end, which will have smaller step sizes.
halfway = total_steps*T/2;
step_size = @(t) min(S, S/((ac_dc_steps+1)*T) * (halfway - abs(t-halfway)));
[COM_X, COM_X_dot] = ALIP_X(step_size, T, h, k_v_x, k_p_x);

% Note the use of the heaviside step function below, which reduces the
% first and last steps to half-steps that start and end at 0 respectively.

% COM X trajectory w.r.t current support foot.
x_traj_s = @(t) COM_X(mod(t,T));
% COM X trajectory w.r.t ground.
x_traj_g = @(t) COM_X(mod(t,T)) + fix(t/T) .* S/2;
% COM X velocity w.r.t ground.
u_traj = @(t) COM_X_dot(mod(t,T));

%% %%%%%%%%%%%%%%%%%%%%%%%
% FOOT SUPPORT LOCATIONS %
%%%%%%%%%%%%%%%%%%%%%%%%%%
x_support = zeros(1, total_steps + 2);
x_support(1:2) = [0, 0];
for i = 1:total_steps-1
    x_support(i+2) = x_support(i+1)+x_traj_store(i,end)-x_traj_store(i+1,1);
end
x_support(total_steps+2) = x_support(total_steps+1);

y_support = zeros(1, total_steps + 2);
for i = 1:total_steps+2
    y_support(i) = (-1)^(i+1) * foot_separation/2;
end

x_zmp_g_store = zeros(total_steps, total_step_timesteps);
x_swing_traj = zeros(total_steps, total_step_timesteps);
y_swing_traj = zeros(total_steps, total_step_timesteps);
z_swing_traj = zeros(total_steps, total_step_timesteps);

x_swing_step = zeros(2, total_steps+1); % start/end of swing step
x_swing_step(1, 1) = 0;
for i = 2:total_steps
    x_swing_step(1, i) = x_traj_store(i-1,end);
    x_swing_step(2, i-1) = x_traj_store(i,1);
end

for i = 1:total_steps
    % use foot placement to find zmp
    x_zmp_g_store(i,:) = x_support(i+1);
    % and trajectory of swing leg relative to the support foot
    [SWING_X, SWING_Z] = swing_leg_traj(x_swing_step(2,i)-x_swing_step(1,i), T, z_max);
    x_swing_traj(i,:) = SWING_X(step_timeline) + x_swing_step(1, i);
    z_swing_traj(i,:) = SWING_Z(step_timeline);
    y_swing_traj(i,:) = (foot_separation-COM_Y(step_timeline))*(-1)^i;
end

%% %%%%%%%%%%%%%%%%%%%%%
% RESULT CONSOLIDATION %
%%%%%%%%%%%%%%%%%%%%%%%%
% In the first step, right leg supports while left swings.
L_x = zeros(total_steps, total_step_timesteps);
L_y = zeros(total_steps, total_step_timesteps);
L_z = zeros(total_steps, total_step_timesteps);
R_x = zeros(total_steps, total_step_timesteps);
R_y = zeros(total_steps, total_step_timesteps);
R_z = zeros(total_steps, total_step_timesteps);

for i  =  1:2:(total_steps-1) % left leg swings
    L_x(i,:) = x_swing_traj(i,:);
    L_y(i,:) = y_swing_traj(i,:);
    L_z(i,:) = z_swing_traj(i,:);

    R_x(i,:) = x_traj_store(i,:);
    R_y(i,:) = COM_Y(step_timeline);
end

for i  =  2:2:total_steps % right leg swings
    L_x(i,:) = x_traj_store(i,:);
    L_y(i,:) = -COM_Y(step_timeline);
    
    R_x(i,:) = x_swing_traj(i,:);
    R_y(i,:) = y_swing_traj(i,:);
    R_z(i,:) = z_swing_traj(i,:);
end

%% %%%%%%%%%%%%%%%%%%%
% INVERSE KINEMATICS %
%%%%%%%%%%%%%%%%%%%%%%
A_r_r = zeros(1, total_timesteps);
A_r_l = zeros(1, total_timesteps);
A_p_r = zeros(1, total_timesteps);
A_p_l = zeros(1, total_timesteps);
K_p_r = zeros(1, total_timesteps);
K_p_l = zeros(1, total_timesteps);
H_r_r = zeros(1, total_timesteps);
H_r_l = zeros(1, total_timesteps);
H_p_r = zeros(1, total_timesteps);
H_p_l = zeros(1, total_timesteps);

L_pos = zeros(3, total_timesteps);
R_pos = zeros(3, total_timesteps);

ind = 1;
for i = 1:total_steps
    for j = 1:length(step_timeline)
        X = L_x(i,j);
        Y = L_y(i,j)+foot_separation/2;
        Z = hip_height-L_z(i,j);
        L_pos(:,ind) = [X,Y,Z];
        [A_r_l(ind),A_p_l(ind),K_p_l(ind),H_r_l(ind),H_p_l(ind)] = IK(X,Y,Z);

        X = R_x(i,j);
        Y = R_y(i,j)-foot_separation/2;
        Z = hip_height-R_z(i,j);
        R_pos(:,ind) = [X,Y,Z];
        [A_r_r(ind),A_p_r(ind),K_p_r(ind),H_r_r(ind),H_p_r(ind)] = IK(X,Y,Z);

        ind = ind + 1;
    end
end

%% %%%%%%
% SCORE %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Scores the strength of the trajectory based on the KE req'd. %
% The lower the score the better.                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
joint_score = @(angle) trapz(gradient(angle).^2);
score = joint_score(A_r_r) + joint_score(A_p_r) + joint_score(K_p_r) + joint_score(H_r_r) + joint_score(H_p_r) + ...
    joint_score(A_r_l) + joint_score(A_p_l) + joint_score(K_p_l) + joint_score(H_r_l) + joint_score(H_p_l);

end
function [x_swing, dx_swing, ddx_swing, z_swing, dz_swing, ddz_swing] = swing_leg_traj(T, S, z_max)
%SWING_LEG_TRAJ Returns the x and z swing leg position and velocity
%     functions relative to the start position of the swing leg.
%     Uses a quartic function determined using the following conditions:
%       / x(0) = 0
%      /  x(T) = S
%     {   x(T/2) = S/2
%      \  x'(0) = 0
%       \ x'(T) = 0
%       / z(0) = 0
%      /  z(T) = 0
%     {   z(T/2) = z_max
%      \  z'(0) = 0
%       \ z'(T) = 0
%
% INPUTS
%     S                     foot distance covered (half the step size)
%     T                     time spent (half the step period)
%     z_max                 maximum desired z_swing height.
% OUTPUTS
%     x                     x swing position function
%     z                     z swing position function

% For the x trajectory we use a general quartic:
%  x(t) = A4t^4 + A3t^3 + A2t^2 + A1t + A0
% Solving for the coefficients:
A0 = 0;
A1 = 0;
A2 = 45/16 * S/T^2;
A3 = -23/16 * S/T^3;
A4 = -3/8 * S/T^4;
x_swing = @(t) A4*t.^4 + A3*t.^3 + A2*t.^2 + A1*t + A0;
dx_swing = @(t) 4*A4*t.^3 + 3*A3*t.^2 + 2*A2*t + A1;
ddx_swing = @(t) 12*A4*t.^2 + 6*A3*t + 2*A2;


% For the z trajectory we use a general quartic:
%  z(t) = B4t^4 + B3t^3 + B2t^2 + B1t + B0
% Solving for the coefficients:
B0 = 0;
B1 = 0;
B2 = 16 * z_max/T^2;
B3 = -32 * z_max/T^3;
B4 = 16 * z_max/T^4;
z_swing = @(t) B4*t.^4 + B3*t.^3 + B2*t.^2 + B1*t + B0;
dz_swing = @(t) 4*B4*t.^3 + 3*B3*t.^2 + 2*B2*t + B1;
ddz_swing = @(t) 12*B4*t.^2 + 6*B3*t + 2*B2;

end

function [x_com, dx_com, ddx_com, y_com, dy_com, ddy_com] = ALIP(T, S, FS, h, K)
%ALIP Returns the position, velocity and acceleration ALIP functions.
%     Uses model:
%     / x'' = g/h * x + F(t), where F(t) = k_v_x*x' + k_p_x*x,
%     \ y'' = g/h * y + F(t), where F(t) = k_v_y*y' + k_p_y*y,
%     where x and y are the horizontal coordinates of the COM position
%     in the sagittal and coronal planes respectively, relative to the
%     support foot.
%     Note that k_v and k_p need not be equal for the saggital and
%     coronal plane models.
%     Setting k_v and k_p to zero reduces the solution back to LIPM.
%     Initial position and velocity are determined assuming a consistent
%     repetitive gait:
%     / x(T) = x(0) + S
%     \ x'(T) = x'(0)
%     / y(0) = FS / 2
%     \ y'(T) = -y'(0)
%
% INPUTS
%     S                     COM distance covered (half the step size)
%     T                     time spent (half the step period)
%     FS                    foot separation
%     h                     COM height (assumed constant)
%     K                     coefficients of augmenting function [x,u,y,v]
% OUTPUTS
%     x_com                 x position function
%     dx_com                x velocity function
%     ddx_com               x acceleration function
%     y_com                 y position function
%     dy_com                y velocity function
%     ddy_com               y acceleration function

% CONSTANTS
g = 9.80665; % m/s^2
% For a general second order ODE, we have:
%  x'' + ax' + bx = 0

% X TRAJECTORY
a = -K(2);
b = -g/h - K(1);
% The resulting equation has three different forms, depending on
% the sign of delta.
delta = a^2 - 4*b;
A = -a / 2;

if delta < 0
    % Determine initial position and velocity.
    B = sqrt(-delta) / 2;
    P = (A^2+B^2)*sin(B*T) / (A*sin(B*T)+B*cos(B*T)-B*exp(-A*T));
    x0 = S / ((cos(B*T)+(P-A)/B*sin(B*T))*exp(A*T) - 1);
    dx0 = P * x0;
    % Compute position and velocity functions.
    x_com = @(t) exp(A*t) .* (x0*cos(B*t) + (dx0-A*x0)/B*sin(B*t));
    dx_com = @(t) exp(A*t) .* (dx0*cos(B*t) + (A*dx0-(A^2+B^2)*x0)/B*sin(B*t));
    ddx_com = @(t) exp(A*t) .* ((2*A*dx0-(A^2+B^2)*x0)*cos(B*t) + ((A^2-B^2)*dx0-A*(A^2+B^2)*x0)/B*sin(B*t));
elseif delta == 0
    % Determine initial position and velocity.
    if A == 0
        x0 = 0;
        dx0 = S / T;
    else
        P = -A^2*T*exp(A*T) / (1-exp(A*T)-A*T*exp(A*T));
        x0 = S / ((P*T-A*T+1)*exp(A*T) - 1);
        dx0 = P * x0;
    end
    % Compute position and velocity functions.
    x_com = @(t) exp(A*t) .* ((dx0-A*x0)*t + x0);
    dx_com = @(t) exp(A*t) .* ((dx0-A*x0)*(1+A*t) + A*x0);
    ddx_com = @(t) exp(A*t) .* ((dx0-A*x0)*(2+A*t)*A + A^2*x0);
else
    % Determine initial position and velocity.
    B = sqrt(delta) / 2;
    r1 = A-B;
    r2 = A+B;
    P = r1*r2 * (exp(r1*T)-exp(r2*T)) / (2*B+r1*exp(r1*T)-r2*exp(r2*T));
    x0 = S / (((r2-P)*exp(r1*T)-(r1-P)*exp(r2*T))/(2*B) - 1);
    dx0 = P * x0;
    % Compute position and velocity functions.
    x_com = @(t) ((r2*x0-dx0)*exp(r1*t)-(r1*x0-dx0)*exp(r2*t))/(2*B);
    dx_com = @(t) ((r2*x0-dx0)*r1*exp(r1*t)-(r1*x0-dx0)*r2*exp(r2*t))/(2*B);
    ddx_com = @(t) ((r2*x0-dx0)*r1^2*exp(r1*t)-(r1*x0-dx0)*r2^2*exp(r2*t))/(2*B);
end

% Y TRAJECTORY
a = -K(4);
b = -g/h - K(3);
% The resulting equation has three different forms, depending on
% the sign of delta.
delta = a^2 - 4*b;
A = -a / 2;

if delta < 0
    % Determine initial position and velocity.
    B = sqrt(-delta) / 2;
    P = (A^2+B^2)*sin(B*T) / (A*sin(B*T)+B*cos(B*T)+B*exp(-A*T));
    y0 = FS / 2;
    dy0 = P * y0;
    % Compute position and velocity functions.
    y_com = @(t) exp(A*t) .* (y0*cos(B*t) + (dy0-A*y0)/B*sin(B*t));
    dy_com = @(t) exp(A*t) .* (dy0*cos(B*t) + (A*dy0-(A^2+B^2)*y0)/B*sin(B*t));
    ddy_com = @(t) exp(A*t) .* ((2*A*dy0-(A^2+B^2)*y0)*cos(B*t) + ((A^2-B^2)*dy0-A*(A^2+B^2)*y0)/B*sin(B*t));
elseif delta == 0
    y0 = FS / 2;
    % Determine initial position and velocity.
    if A == 0
        dy0 = 0;
    else
        P = A^2*T*exp(A*T) / ((1+A*T)*exp(A*T) + 1);
        dy0 = P * y0;
    end
    % Compute position and velocity functions.
    y_com = @(t) exp(A*t) .* ((dy0-A*y0)*t + y0);
    dy_com = @(t) exp(A*t) .* ((dy0-A*y0)*(1+A*t) + A*y0);
    ddy_com = @(t) exp(A*t) .* ((dy0-A*y0)*(2+A*t)*A + A^2*y0);
else
    % Determine initial position and velocity.
    B = sqrt(delta) / 2;
    r1 = A-B;
    r2 = A+B;
    P = r1*r2 * (exp(r2*T)-exp(r1*T)) / (r2*exp(r2*T)-r1*exp(r1*T)+2*B);
    y0 = FS / 2;
    dy0 = P * y0;
    % Compute position and velocity functions.
    y_com = @(t) ((r2*y0-dy0)*exp(r1*t)-(r1*y0-dy0)*exp(r2*t))/(2*B);
    dy_com = @(t) ((r2*y0-dy0)*r1*exp(r1*t)-(r1*y0-dy0)*r2*exp(r2*t))/(2*B);
    ddy_com = @(t) ((r2*y0-dy0)*r1^2*exp(r1*t)-(r1*y0-dy0)*r2^2*exp(r2*t))/(2*B);
end

end
